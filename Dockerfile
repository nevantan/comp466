#########################
# Build React front-end #
#########################
FROM node:10.12.0 as yarnbuilder

COPY --chown=node:node ./src/main/react/ /home/node/src
WORKDIR /home/node/src
RUN yarn install
RUN yarn build -o ./bundle.js

####################
# Build Tomcat WAR #
####################

FROM gradle:jdk10 as gradlebuilder

COPY --chown=gradle:gradle . /home/gradle/src
COPY --chown=gradle:gradle --from=yarnbuilder /home/node/src/bundle.js /home/gradle/src/src/main/webapp/dist/bundle.js
WORKDIR /home/gradle/src
RUN gradle build

################
# Start server #
################

FROM tomcat:9.0.11-jre10

COPY --from=gradlebuilder /home/gradle/src/build/libs/comp466.war /usr/local/tomcat/webapps/ROOT.war

#ADD ./build/libs/comp466.war /usr/local/tomcat/webapps/ROOT.war

RUN rm -rf /usr/local/tomcat/webapps/ROOT
CMD ["catalina.sh", "run"]