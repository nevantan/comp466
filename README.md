# COMP466 Challenge Project

This repo contains an elearning system that may be deployed using docker or manually.

The system was built and has been thoroughly tested on:

- Ubuntu 18.04
- Ubuntu 16.04

The system has been tested and confirmed to work (via Docker) on:

- Google Container-Optimized OS
- Windows 10 Professional

# Getting Started

The system may be deployed using either docker or manually. By using docker, the server
will be running in a known and consistent environment and will not require build dependencies
to deploy.

## Docker Deployment (Recommended)

Using docker, the images may be built locally or simply pulled from the repository.

### Pull Images (Recommended)

1.  Install Docker for your system:

    - [Windows](https://docs.docker.com/docker-for-windows/install/)
    - [Mac (Untested)](https://docs.docker.com/docker-for-mac/install/)
    - [Linux (Ubuntu)](https://docs.docker.com/install/linux/docker-ce/ubuntu/) (Recommended)

2.  If using a Linux distribution, install [Docker Compose](https://docs.docker.com/compose/install/) (this is pre-installed
    with the Mac and Windows versions)

3.  Create a docker volume called 'data' to persist MongoDB data: `docker volume create data`

4.  Clone the respository and enter the directory:

```shell
git clone https://gitlab.com/nevantan/comp466.git
cd comp466
```

5.  Start the cluster: `docker-compose up`

The cluter will take some time to start up the first time as it is pulling the docker images before starting the cluster.

The app will be available on http://127.0.0.1:8080 - note that starting the cluter will fail if there is something else
running on port 8080.

### Build Images

1.  Follow steps 1, 2, 3, and 4 above

2.  Modify the docker-compose.yml file to match:

```yml
version: '3.3'

volumes:
  data:

services:
  tomcat:
    container_name: tomcat
    restart: always
    build: .
    volumes:
      - ./courses:/courses
    ports:
      - '8080:8080'
    links:
      - mongo
  mongo:
    container_name: mongo
    image: mongo
    volumes:
      - data:/data/db
    ports:
      - '27017:27017'

  mongo-seed:
    build: ./database
    links:
      - mongo
```

(It's just changing two of the 'image' directives to 'build')

3.  Start the cluster: `docker-compose up`

## Manual Deployment

Please note that this method has only been tested on Ubuntu 18.04. Additionally, due to variability across systems, this
method may require modifications in order to function properly on your own system.

1.  Install the dependencies:

    - Docker (see above for links)
    - [NodeJS](https://nodejs.org/en/)
    - [MongoDB](https://www.mongodb.com/)
    - [Apache Tomcat](http://tomcat.apache.org/)

2.  Clone the repository and enter the directory:

```shell
git clone https://gitlab.com/nevantan/comp466.git
cd comp466
```

3.  Modify the `src/main/resources/config.ini` file to ensure the database connection details and file paths are correct for your system

4.  Build the React app:

```shell
cd src/main/react/
npm install && npm run build
cd ../../../
```

5.  Build the Servlet app:

```shell
./gradlew build
```

6.  Deploy the WAR file `build/libs/comp466.war` and view the app (URL will vary according do your Tomcat configuration)
