// Intercept form submit
const loginform = document.getElementById('login-form');
const onSubmit = (e) => {
  e.preventDefault();

  const emailField = document.getElementById('email');
  const passwordField = document.getElementById('password');
  let error = false;

  if (emailField.value === '') {
    emailField.classList.add('error');
    error = true;
  }

  if (passwordField.value === '') {
    passwordField.classList.add('error');
    error = true;
  }

  if (error) {
    const errorMessage = document.createElement('div');
    errorMessage.innerText = 'Email and Password fields cannot be blank.';
    errorMessage.classList.add('error');
    document.getElementById('errorwrap').appendChild(errorMessage);
    return false;
  }

  loginform.removeEventListener('submit', onSubmit);
  loginform.submit();
};

loginform.addEventListener('submit', onSubmit);
