<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="content-type" content="text/html" charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>eLearning Login</title>

  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500|Open+Sans" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="/dist/style.css" />

  <script src="/dist/login.js" async></script>
</head>
<body>
  <header>
    <h1>eLearning System</h1>
  </header>
  <div class="card">
    <h2>Login</h2>
    <div id="errorwrap">
      <%
        if(request.getAttribute("errorMessage") != null) {
          out.println("<div class=\"error\">" + request.getAttribute("errorMessage") + "</div>");
        }
      %>
    </div>
    <form action="/auth" method="POST" id="login-form">
      <label for="email">Email:</label>
      <input id="email" type="email" name="email" placeholder="Enter your email address" value='<% out.println(request.getAttribute("email")); %>' />

      <label for="password">Password:</label>
      <input id="password" type="password" name="password" placeholder="Enter your password" />

      <input type="submit" value="Login" />
    </form>
  </div>
</body>
</html>