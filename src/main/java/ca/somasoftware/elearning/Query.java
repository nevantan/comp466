package ca.somasoftware.elearning;

import java.util.ArrayList;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoCursor;
import org.bson.Document;

import graphql.GraphQLException;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

public class Query {
  public static DataFetcher getCourseResolver() {
    return new DataFetcher<Course>() {
      @Override
      public Course get(DataFetchingEnvironment env) {
        String id = env.getArgument("id");
        return new Course(id);
      }
    };
  }

  public static DataFetcher getUserResolver(MongoClient client) {
    return new DataFetcher<User>() {
      @Override
      public User get(DataFetchingEnvironment env) {
        User requester = new User(client, env.getContext());
        String uid = env.getArgument("id");

        if(!requester.getIsTeacher()) {
          if(!uid.equals((String)env.getContext())) {
            throw new GraphQLException("You do not have permission to access this user.");
          }
        }

        return new User(client, uid);
      }
    };
  }

  public static DataFetcher getUsersResolver(MongoClient client) {
    return new DataFetcher<User[]>() {
      @Override
      public User[] get(DataFetchingEnvironment env) {
        String uid = env.getContext();
        User requester = new User(client, uid);

        if(!requester.getIsTeacher()) {
          throw new GraphQLException("You do not have permisson to make this query.");
        }

        MongoCursor<Document> cursor = client
          .getDatabase("elearning")
          .getCollection("users")
          .find()
          .iterator();

        ArrayList<User> users = new ArrayList<User>();

        try {
          while(cursor.hasNext()) {
            users.add(new User(cursor.next()));
          }
        } finally {
          cursor.close();
        }

        return users.toArray(new User[0]);
      }
    };
  }
}