package ca.somasoftware.elearning;

import org.json.JSONObject;
import org.json.JSONException;
import org.json.HTTP;

import java.io.*;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;

import graphql.ExecutionInput;
import graphql.ExecutionResult;
import graphql.GraphQL;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.TypeDefinitionRegistry;

@WebServlet(name = "GraphQLServlet", urlPatterns = {"/graphql"}, loadOnStartup = 1)
public class GraphQLServlet extends HttpServlet {
  private GraphQL gql;
  private MongoClient mongoClient;

  public void init() throws ServletException {
    HashMap<String, String> config = readConfig("config.ini");

    // Init MongoDB client
    mongoClient = MongoClients.create(config.get("MONGO_CONNECTION_STRING"));

    // Init GraphQL
    try {
      String schemaString = readSchema("schema.graphqls");
      SchemaParser parser = new SchemaParser();
      TypeDefinitionRegistry registry = parser.parse(schemaString);

      RuntimeWiring wiring = buildRuntimeWiring();

      SchemaGenerator generator = new SchemaGenerator();
      GraphQLSchema graphQLSchema = generator.makeExecutableSchema(registry, wiring);

      gql = GraphQL.newGraphQL(graphQLSchema).build();
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    StringBuffer jb = new StringBuffer();
    String line = null;
    try {
      BufferedReader reader = request.getReader();
      while((line = reader.readLine()) != null) jb.append(line);
    } catch(Exception e) { e.printStackTrace(); }

    try {
      JSONObject jsonObject = new JSONObject(jb.toString());

      String query = jsonObject.getString("query");

      JSONObject vars;
      try {
        vars = jsonObject.getJSONObject("variables");
      } catch(JSONException e) {
        vars = new JSONObject("{}");
      }

      Map<String, Object> variables = new Gson().fromJson(vars.toString(), Map.class);

      ExecutionInput input = ExecutionInput
        .newExecutionInput()
        .query(query)
        .variables(variables)
        .context(request.getAttribute("userId"))
        .build();
      ExecutionResult result = gql.execute(input);

      Gson gson = new Gson();
      Map<String, Object> toSpecificationResult = result.toSpecification();

      setHeaders(response);
      response.getWriter().print(gson.toJson(toSpecificationResult));
    } catch(JSONException e) {
      e.printStackTrace();
      throw new IOException("Error parsing JSON request string");
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  @Override
  protected void doOptions(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    setHeaders(response);
    response.setStatus(HttpServletResponse.SC_OK);
  }

  private void setHeaders(HttpServletResponse response) {
    response.setHeader("Content-Type", "application/json");
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader(
        "Access-Control-Allow-Headers", "Connection, Host, Origin, Referer, " +
        "Access-Control-Request-Method, Access-Control-Request-Headers, User-Agent, Accept, " +
        "Content-Type, Authorization, Content-Length, X-Requested-With, Accept-Encoding, " +
        "Accept-Language, Authorization"
      );
    response.setHeader("Access-Control-Allow-Methods", "POST, OPTIONS");
  }

  private String readSchema(String filename) {
    StringBuilder result = new StringBuilder("");

    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(filename).getFile());

    try(Scanner scanner = new Scanner(file)) {
      while(scanner.hasNextLine()) {
        String line = scanner.nextLine();
        result.append(line).append("");
      }

      scanner.close();
    } catch(IOException e) {
      e.printStackTrace();
    }

    return result.toString();
  }

  private HashMap<String, String> readConfig(String filename) {
    HashMap<String, String> result = new HashMap<String, String>();

    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(filename).getFile());

    try(Scanner scanner = new Scanner(file)) {
      while(scanner.hasNextLine()) {
        String line = scanner.nextLine();
        String[] keyval = line.split("=");
        result.put(keyval[0], keyval[1]);
      }

      scanner.close();
    } catch(IOException e) {
      e.printStackTrace();
    }

    return result;
  }

  private RuntimeWiring buildRuntimeWiring() {
    return RuntimeWiring.newRuntimeWiring()
      .type("Query", builder -> builder
        .dataFetcher("course", Query.getCourseResolver())
        .dataFetcher("user", Query.getUserResolver(mongoClient))
        .dataFetcher("users", Query.getUsersResolver(mongoClient))
      )
      .type("Mutation", builder -> builder
        .dataFetcher("gradeQuiz", Mutation.getGradeQuizResolver(mongoClient))
        .dataFetcher("addUser", Mutation.getAddUserResolver(mongoClient))
        .dataFetcher("removeUser", Mutation.getRemoveUserResolver(mongoClient))
        .dataFetcher("setIsTeacher", Mutation.getSetIsTeacherResolver(mongoClient))
        .dataFetcher("enrollCourse", Mutation.getEnrollCourseResolver(mongoClient))
        .dataFetcher("withdrawCourse", Mutation.getWithdrawCourseResolver(mongoClient))
        .dataFetcher("resetPassword", Mutation.getPasswordResetResolver(mongoClient))
      )
      .type("Course", builder -> builder.dataFetcher("unit", Course.getUnitResolver()))
      .type("Unit", builder -> builder
        .dataFetcher("page", Unit.getPageResolver())
        .dataFetcher("lesson", Unit.getLessonResolver())
        .dataFetcher("quiz", Unit.getQuizResolver())
      )
      .type("User", builder -> builder
        .dataFetcher("enrolments", User.getEnrolmentsResolver())
        .dataFetcher("quiz", User.getQuizResolver())
        .dataFetcher("password", User.getPasswordResolver())
      )
      .build();
  }
}
