package ca.somasoftware.elearning;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.Map;
import java.util.HashMap;
import java.util.Scanner;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.RequestDispatcher;

import java.security.Key;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

import com.google.common.hash.Hashing;

import org.bson.Document;
import org.bson.types.ObjectId;
import com.mongodb.client.MongoClients;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.eq;

@WebServlet(name = "Auth", urlPatterns = {"/auth"}, loadOnStartup = 1)
public class Auth extends HttpServlet {
  private MongoClient mongoClient;

  public void init() throws ServletException {
    // Init MongoDB client
    mongoClient = MongoClients.create("mongodb://mongo:27017");
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String email = request.getParameter("email").toLowerCase();
    String password = request.getParameter("password");
    Key key = (Key)request.getAttribute("key");

    String mode = request.getParameter("mode");

    try {
      if(email == null || password == null) throw new Exception("Must enter both email and password.");

      Document user = mongoClient.getDatabase("elearning").getCollection("users").find(eq("email", email)).first();

      if(user == null) throw new Exception("Email does not exist: " + email);

      String id = user.get("_id").toString();

      String passHash = Hashing.sha256().hashString(id + user.getString("email") + password, StandardCharsets.UTF_8).toString();
      if(user.getBoolean("temp_pass")) {
        passHash = password;
      }

      if(!user.getString("password").equals(passHash)) throw new Exception("Incorrect password.");

      String jws = Jwts.builder().setSubject(id).signWith(key).compact();
      request.setAttribute("token", jws);

      if(mode != null && mode.equals("api")) {
        response.setStatus(HttpServletResponse.SC_OK);
        setHeaders(response);
        response.setHeader("Content-Type", "text/plain");
        response.getWriter().print(jws);
      } else {
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/dashboard.jsp");
        dispatcher.forward(request, response);
      }
    } catch(Exception e) {
      if(mode != null && mode.equals("api")) {
        response.setStatus(HttpServletResponse.SC_FORBIDDEN);
        setHeaders(response);
        response.setHeader("Content-Type", "text/plain");
        response.getWriter().print("FORBIDDEN");
      } else {
        request.setAttribute("errorMessage", "Invalid email/password combination.");
        request.setAttribute("email", email);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/login.jsp");
        dispatcher.forward(request, response);
      }
    }
  }

  private void setHeaders(HttpServletResponse response) {
    response.setHeader("Content-Type", "application/json");
    response.setHeader("Access-Control-Allow-Origin", "*");
    response.setHeader(
        "Access-Control-Allow-Headers", "Connection, Host, Origin, Referer, " +
        "Access-Control-Request-Method, Access-Control-Request-Headers, User-Agent, Accept, " +
        "Content-Type, Authorization, Content-Length, X-Requested-With, Accept-Encoding, " +
        "Accept-Language, Authorization"
      );
    response.setHeader("Access-Control-Allow-Methods", "POST, OPTIONS");
  }
}
