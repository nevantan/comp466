package ca.somasoftware.elearning;

import java.io.IOException;
import java.util.regex.Pattern;
import java.util.regex.Matcher;

import javax.servlet.ServletException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.RequestDispatcher;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.security.Key;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.security.Keys;

@WebFilter("/*")
public class Router implements Filter {
  private Key key;

  @Override
  public void init(FilterConfig config) {
    key = Keys.secretKeyFor(SignatureAlgorithm.HS256);
  }

  @Override
  public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain) throws IOException, ServletException {
    HttpServletRequest request = (HttpServletRequest)req;
    HttpServletResponse response = (HttpServletResponse)res;
    String requestUrl = request.getRequestURL().toString();

    if(request.getMethod().equalsIgnoreCase("POST")) {
      if(Pattern.compile("/auth").matcher(requestUrl).find()) {
        request.setAttribute("key", key);
        request.getRequestDispatcher("/auth").forward(request, response);
      } else if(Pattern.compile("/graphql").matcher(requestUrl).find()) {
        if(authenticated(request)) {
          req.setAttribute("userId", parseJwt(request));
          chain.doFilter(req, res);
        } else {
          response.setStatus(HttpServletResponse.SC_FORBIDDEN);
          response.getWriter().print("You are not authorized to query this server.");
        }
      }
    } else {
      if(validUrl(requestUrl)) {
        chain.doFilter(req, res);
      } else {
        if(authenticated(request)) {
          request.getRequestDispatcher("/dashboard.jsp").forward(request, response);
        } else {
          request.setAttribute("errorMessage", null);
          request.setAttribute("email", "");
          request.getRequestDispatcher("/login.jsp").forward(request, response);
        }
      }
    }
  }

  private Boolean validUrl(String url) {
    if(Pattern.compile("dist/").matcher(url).find()) {
      return true;
    } else if(Pattern.compile("/graphql").matcher(url).find()) {
      return true;
    }

    return false;
  }

  private Boolean authenticated(HttpServletRequest request) {
    String token = request.getHeader("Authorization");
    if(token == null) { // No header, fallback to cookie
      token = getTokenFromCookie(request.getCookies());
    }

    if(token != null) {
      try {
        Jws<Claims> jws = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
        return true;
      } catch(JwtException e) {
        return false;
      }
    } else {
      return false;
    }
  }

  private String parseJwt(HttpServletRequest request) {
    String token = request.getHeader("Authorization");
    if(token == null) { // No header, fallback to cookie
      token = getTokenFromCookie(request.getCookies());
    }

    if(token != null) {
      try {
        Jws<Claims> jws = Jwts.parser().setSigningKey(key).parseClaimsJws(token);
        return jws.getBody().getSubject();
      } catch(JwtException e) {
        return null;
      }
    } else {
      return null;
    }
  }

  private String getTokenFromCookie(Cookie[] cookies) {
    if(cookies == null) return null;

    for(int i = 0; i < cookies.length; i++) {
      if(cookies[i].getName().equals("jwt")) return cookies[i].getValue();
    }
    return null;
  }
}