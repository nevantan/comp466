package ca.somasoftware.elearning;

import java.util.HashMap;
import java.util.LinkedHashMap;

import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

public class Unit {
  private String id;
  private String title;
  private LinkedHashMap<String, Page> pages = new LinkedHashMap<String, Page>();
  private LinkedHashMap<String, Lesson> lessons = new LinkedHashMap<String, Lesson>();
  private LinkedHashMap<String, Quiz> quizzes = new LinkedHashMap<String, Quiz>();

  public Unit() {}

  public Unit(String id, String title) {
    this.id = id;
    this.title = title;
  }

  public Unit(Element el) {
    NamedNodeMap attrs = el.getAttributes();
    id = attrs.getNamedItem("id").getNodeValue();
    title = attrs.getNamedItem("title").getNodeValue();

    NodeList pageNodes = ((Node)el).getChildNodes();
    Node page = null;
    for(int i = 0; i < pageNodes.getLength(); i++) {
      page = pageNodes.item(i);
      switch(page.getNodeName()) {
        case "lesson":
          addLesson((Element)page);
          break;
        case "quiz":
          addQuiz((Element)page);
          break;
      }
    }

    // NodeList lessonNodes = el.getElementsByTagName("lesson");
    // Element lesson = null;
    // for(int i = 0; i < lessonNodes.getLength(); i++) {
    //   addLesson((Element)lessonNodes.item(i));
    //   // lesson = (Element)lessonNodes.item(i);
    //   // lessons.put(lesson.getAttributes().getNamedItem("id").getNodeValue(), new Lesson((Element)lesson));
    // }

    // NodeList quizNodes = el.getElementsByTagName("quiz");
    // Element quiz = null;
    // for(int i = 0; i < quizNodes.getLength(); i++) {
    //   addQuiz((Element)quizNodes.item(i));
    //   // quiz = (Element)lessonNodes.item(i);
    //   // quizzes.put(quiz.getAttributes().getNamedItem("id").getNodeValue(), new Quiz((Element)quiz));
    // }
  }

  private void addLesson(Element lesson) {
    lessons.put(lesson.getAttributes().getNamedItem("id").getNodeValue(), new Lesson((Element)lesson));
    pages.put(lesson.getAttributes().getNamedItem("id").getNodeValue(), new Lesson((Element)lesson));
  }

  private void addQuiz(Element quiz) {
    quizzes.put(quiz.getAttributes().getNamedItem("id").getNodeValue(), new Quiz((Element)quiz));
    pages.put(quiz.getAttributes().getNamedItem("id").getNodeValue(), new Quiz((Element)quiz));
  }

  public String getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public Page getPage(String id) {
    return pages.get(id);
  }

  public Page[] getPages() {
    return pages.values().toArray(new Page[0]);
  }

  public Lesson getLesson(String id) {
    return lessons.get(id);
  }

  public Lesson[] getLessons() {
    return lessons.values().toArray(new Lesson[0]);
  }

  public Quiz getQuiz(String id) {
    return quizzes.get(id);
  }

  public Quiz[] getQuizzes() {
    return quizzes.values().toArray(new Quiz[0]);
  }

  public static DataFetcher getPageResolver() {
    return new DataFetcher<Page>() {
      @Override
      public Page get(DataFetchingEnvironment env) {
        String id = env.getArgument("id");
        return ((Unit)env.getSource()).getPage(id);
      }
    };
  }

  public static DataFetcher getLessonResolver() {
    return new DataFetcher<Lesson>() {
      @Override
      public Lesson get(DataFetchingEnvironment env) {
        String id = env.getArgument("id");
        return ((Unit)env.getSource()).getLesson(id);
      }
    };
  }

  public static DataFetcher getQuizResolver() {
    return new DataFetcher<Quiz>() {
      @Override
      public Quiz get(DataFetchingEnvironment env) {
        String id = env.getArgument("id");
        return ((Unit)env.getSource()).getQuiz(id);
      }
    };
  }
}