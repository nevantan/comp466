package ca.somasoftware.elearning;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

public class Lesson extends Page {
  private ArrayList<Section> sections = new ArrayList<Section>();

  public Lesson() {}

  public Lesson(String id, String title) {
    super(id, "lesson", title);
  }

  public Lesson(Element el) {
    super(el);

    NamedNodeMap attrs = el.getAttributes();
    id = attrs.getNamedItem("id").getNodeValue();
    type = "lesson";
    title = attrs.getNamedItem("title").getNodeValue();

    NodeList children = el.getElementsByTagName("section");
    Element section = null;
    for(int i = 0; i < children.getLength(); i++) {
      section = (Element)children.item(i);
      sections.add(new Section(section));
    }
  }

  public Section[] getSections() {
    return sections.toArray(new Section[0]);
  }
}