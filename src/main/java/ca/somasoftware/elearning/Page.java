package ca.somasoftware.elearning;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

public class Page {
  protected String id;
  protected String type;
  protected String title;

  public Page() {}

  public Page(String id, String type, String title) {
    this.id = id;
    this.type = type;
    this.title = title;
  }

  public Page(Element el) {}

  public String getId() {
    return id;
  }

  public String getType() {
    return type;
  }

  public String getTitle() {
    return title;
  }
}