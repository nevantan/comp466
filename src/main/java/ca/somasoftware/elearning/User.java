package ca.somasoftware.elearning;

import java.util.List;
import java.util.ArrayList;

import com.mongodb.ConnectionString;
import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;

import static com.mongodb.client.model.Filters.*;

import org.bson.*;
import org.bson.types.ObjectId;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

public class User {
  private MongoClient mongoClient;
  private MongoDatabase db;

  private String id;
  private String email;
  private Boolean isTeacher;
  private Boolean isTempPass;
  private String password; // hashed
  private String[] enrolments;

  public User(MongoClient client) {
    mongoClient = client;
  }

  public User(MongoClient client, String id) { // Load by ID
    this(client);

    try {
      MongoCollection<Document> users = getUsersCollection();
      Document user = users.find(eq("_id", new ObjectId(id))).first();

      this.id = id;
      this.email = user.getString("email");
      this.isTeacher = user.getBoolean("isTeacher");
      this.password = user.getString("password");
      this.enrolments = ((List<String>)user.get("enrolments")).toArray(new String[0]);
      this.isTempPass = user.getBoolean("temp_pass");
    } catch(Exception e) {
      e.printStackTrace();
    }
  }

  public User(Document user) {
    this.id = user.get("_id").toString();
    this.email = user.getString("email");
    this.isTeacher = user.getBoolean("isTeacher");
    this.password = user.getString("password");
    this.enrolments = ((List<String>)user.get("enrolments")).toArray(new String[0]);
    this.isTempPass = user.getBoolean("temp_pass");
  }

  public User(MongoClient client, String email, String password, Boolean register) { // Login/register
    this(client);
  }

  public String getId() {
    return this.id;
  }

  public String getEmail() {
    return this.email;
  }

  public Boolean getIsTeacher() {
    return this.isTeacher;
  }

  public Boolean getIsTempPass() {
    return this.isTempPass;
  }

  public String getPassword() {
    return this.password;
  }

  public Boolean getUsingTempPass() {
    return this.isTempPass;
  }

  public String[] getEnrolmentsIds() {
    return this.enrolments;
  }

  public MongoClient getMongoClient() {
    return this.mongoClient;
  }

  private MongoCollection<Document> getUsersCollection() {
    db = mongoClient.getDatabase("elearning");
    return db.getCollection("users");
  }

  public static DataFetcher getPasswordResolver() {
    return new DataFetcher<String>() {
      @Override
      public String get(DataFetchingEnvironment env) {
        User user = (User)env.getSource();

        if(user.getIsTempPass()) {
          return user.getPassword();
        }

        return "************";
      }
    };
  }

  public static DataFetcher getEnrolmentsResolver() {
    return new DataFetcher<Course[]>() {
      @Override
      public Course[] get(DataFetchingEnvironment env) {
        String[] courseIds = ((User)env.getSource()).getEnrolmentsIds();
        Course[] courses = new Course[courseIds.length];

        for(int i = 0; i < courseIds.length; i++) {
          courses[i] = new Course(courseIds[i]);
        }

        return courses;
      }
    };
  }

  public static DataFetcher getQuizResolver() {
    return new DataFetcher<Quiz>() {
      @Override
      public Quiz get(DataFetchingEnvironment env) {
        User user = (User)env.getSource();
        String userId = user.getId();
        String cid = env.getArgument("cid");
        String uid = env.getArgument("uid");
        String pid = env.getArgument("pid");

        MongoCollection quizzes = user
          .getMongoClient()
          .getDatabase("elearning")
          .getCollection("quizzes");

        BsonDocument filter = new BsonDocument()
          .append("user_id", new BsonString(userId))
          .append("cid", new BsonString(cid))
          .append("uid", new BsonString(uid))
          .append("pid", new BsonString(pid));

        Document quizDoc = (Document)quizzes.find(filter).first();

        Quiz quiz = new Quiz("", "");

        if(quizDoc != null && quizDoc.containsKey("responses")) {
          String[] responses = ((ArrayList<String>)quizDoc.get("responses")).toArray(new String[0]);
          Document results = (Document)quizDoc.get("results");
          int correct = ((Number)results.get("correct")).intValue();
          double grade = results.getDouble("grade");

          quiz = new Quiz(pid, responses, correct, grade);
        }

        return quiz;
      }
    };
  }
}