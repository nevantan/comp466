package ca.somasoftware.elearning;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

public class Question {
  private String id;
  private String prompt;
  private String answer;
  private Boolean ordered;

  private ArrayList<Response> responses = new ArrayList<Response>();

  public Question() {}

  public Question(String id, String prompt, String answer, Boolean ordered) {
    this.id = id;
    this.prompt = prompt;
    this.answer = answer;
    this.ordered = ordered;
  }

  public Question(Element el) {
    NamedNodeMap attrs = el.getAttributes();
    id = attrs.getNamedItem("id").getNodeValue();
    prompt = attrs.getNamedItem("prompt").getNodeValue();
    answer = attrs.getNamedItem("answer").getNodeValue();
    ordered = Boolean.parseBoolean(attrs.getNamedItem("ordered").getNodeValue());

    NodeList children = el.getElementsByTagName("response");
    Element response = null;
    for(int i = 0; i < children.getLength(); i++) {
      response = (Element)children.item(i);
      responses.add(new Response(response));
    }
  }

  public String getId() {
    return id;
  }

  public String getPrompt() {
    return prompt;
  }

  public String getAnswer() {
    return answer;
  }

  public Boolean getOrdered() {
    return ordered;
  }

  public Response[] getResponses() {
    return responses.toArray(new Response[0]);
  }
}