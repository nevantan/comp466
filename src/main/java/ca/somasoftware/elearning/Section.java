package ca.somasoftware.elearning;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

public class Section {
  private String title;
  private String content;

  public Section() {}

  public Section(String title, String content) {
    this.title = title;
    this.content = content;
  }

  public Section(Element el) {
    NamedNodeMap attrs = el.getAttributes();
    title = attrs.getNamedItem("title").getNodeValue();
    content = el.getTextContent();
  }

  public String getTitle() {
    return title;
  }

  public String getContent() {
    return content;
  }
}