package ca.somasoftware.elearning;

import java.io.*;
import java.util.HashMap;
import java.util.Scanner;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Element;
import java.io.File;

import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;

public class Course {
  private String id;
  private String title;
  private String code;
  private String description;
  private HashMap<String, Unit> units = new HashMap<String, Unit>();

  private Boolean valid = false;

  public Course(String id) {
    try {
      HashMap<String, String> config = readConfig("config.ini");

      ClassLoader classLoader = getClass().getClassLoader();
      String filename = config.get("COURSE_DIRECTORY") + id + ".xml";
      File xmlFile = new File(filename);
      this.id = id;

      DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
      DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
      Document doc = dBuilder.parse(xmlFile);

      doc.getDocumentElement().normalize();

      NamedNodeMap attrs = doc.getDocumentElement().getAttributes();
      id = attrs.getNamedItem("id").getNodeValue();
      title = attrs.getNamedItem("title").getNodeValue();
      code = attrs.getNamedItem("code").getNodeValue();
      description = attrs.getNamedItem("description").getNodeValue();

      NodeList children = doc.getElementsByTagName("unit");
      Element unit = null;
      for(int i = 0; i < children.getLength(); i++) {
        unit = (Element)children.item(i);
        units.put(unit.getAttributes().getNamedItem("id").getNodeValue(), new Unit((Element)unit));
      }

      valid = true;
    } catch(Exception e) {
      valid = false;
      System.out.println("Invalid course: " + id);
    }
  }

  private String getContents(Document doc, String nodeName) {
    Node node = doc.getElementsByTagName(nodeName).item(0);
    if(node.getNodeType() == Node.ELEMENT_NODE) {
      Element el = (Element)node;
      return el.getTextContent();
    }
    return "";
  }

  public String getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public String getCode() {
    return code;
  }

  public String getDescription() {
    return description;
  }

  public Unit getUnit(String id) {
    return units.get(id);
  }

  public Unit[] getUnits() {
    return units.values().toArray(new Unit[0]);
  }

  public Boolean isValid() {
    return this.valid;
  }

  public static DataFetcher getUnitResolver() {
    return new DataFetcher<Unit>() {
      @Override
      public Unit get(DataFetchingEnvironment env) {
        String id = env.getArgument("id");
        return ((Course)env.getSource()).getUnit(id);
      }
    };
  }

  private HashMap<String, String> readConfig(String filename) {
    HashMap<String, String> result = new HashMap<String, String>();

    ClassLoader classLoader = getClass().getClassLoader();
    File file = new File(classLoader.getResource(filename).getFile());

    try(Scanner scanner = new Scanner(file)) {
      while(scanner.hasNextLine()) {
        String line = scanner.nextLine();
        String[] keyval = line.split("=");
        result.put(keyval[0], keyval[1]);
      }

      scanner.close();
    } catch(IOException e) {
      e.printStackTrace();
    }

    return result;
  }
}