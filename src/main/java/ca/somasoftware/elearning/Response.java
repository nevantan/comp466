package ca.somasoftware.elearning;

import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;

public class Response {
  private String id;
  private String text;

  public Response() {}

  public Response(String id, String text) {
    this.id = id;
    this.text = text;
  }

  public Response(Element el) {
    NamedNodeMap attrs = el.getAttributes();
    id = attrs.getNamedItem("id").getNodeValue();
    text = el.getTextContent();
  }

  public String getId() {
    return id;
  }

  public String getText() {
    return text;
  }
}