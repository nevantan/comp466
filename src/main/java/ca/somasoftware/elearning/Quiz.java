package ca.somasoftware.elearning;

import java.util.ArrayList;

import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.w3c.dom.NamedNodeMap;

public class Quiz extends Page {
  private ArrayList<Question> questions = new ArrayList<Question>();
  private String[] responses;
  private int correct;
  private double grade;

  public Quiz() {}

  public Quiz(String id, String title) {
    super(id, "quiz", title);
  }

  public Quiz(String id, String[] responses, int correct, double grade) {
    super(id, "quiz", "");
    this.responses = responses;
    this.correct = correct;
    this.grade = grade;
  }

  public Quiz(Element el) {
    super(el);

    NamedNodeMap attrs = el.getAttributes();
    id = attrs.getNamedItem("id").getNodeValue();
    type = "quiz";
    title = attrs.getNamedItem("title").getNodeValue();

    NodeList children = el.getElementsByTagName("question");
    Element question = null;
    for(int i = 0; i < children.getLength(); i++) {
      question = (Element)children.item(i);
      questions.add(new Question(question));
    }
  }

  public Question[] getQuestions() {
    return questions.toArray(new Question[0]);
  }

  public String[] getResponses() {
    return responses;
  }

  public int getTotal() {
    return questions.size();
  }

  public int getCorrect() {
    return correct;
  }

  public Boolean getPass() {
    return grade >= 0.5;
  }

  public double getGrade() {
    return grade;
  }
}