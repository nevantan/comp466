package ca.somasoftware.elearning;

import java.util.Arrays;
import java.util.ArrayList;
import java.nio.charset.StandardCharsets;

import com.mongodb.client.MongoClient;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.MongoCollection;
import static com.mongodb.client.model.Filters.*;

import org.bson.*;
import org.bson.types.ObjectId;

import com.google.common.hash.Hashing;

import graphql.*;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.execution.DataFetcherResult;

public class Mutation {
  public static DataFetcher getGradeQuizResolver(MongoClient client) {
    return new DataFetcher<Boolean[]>() {
      @Override
      public Boolean[] get(DataFetchingEnvironment env) {
        String id = env.getContext();

        String cid = env.getArgument("cid");
        String uid = env.getArgument("uid");
        String qid = env.getArgument("qid");
        ArrayList<String> responses = env.getArgument("answers");

        // If user doesn't have access to this course
        User user = new User(client, id);
        String[] enrolments = user.getEnrolmentsIds();
        if(!Arrays.asList(enrolments).contains(cid)) {
          // Throw auth error
          throw new GraphQLException("User is not enroled in this course.");
        }

        // If user has already submitted this quiz
        MongoDatabase db = client.getDatabase("elearning");
        MongoCollection quizzes = db.getCollection("quizzes");
        BsonDocument filter = new BsonDocument()
          .append("user_id", new BsonString(id))
          .append("cid", new BsonString(cid))
          .append("uid", new BsonString(uid))
          .append("pid", new BsonString(qid));
        Document userQuiz = (Document)quizzes.find(filter).first();
        if(userQuiz != null) {
          // Throw already submitted error
          throw new GraphQLException("User has already submitted this quiz; no further attempts are allowed.");
        }

        Course course = new Course(cid);
        Unit unit = course.getUnit(uid);
        Quiz quiz = unit.getQuiz(qid);

        Question[] questions = quiz.getQuestions();
        Boolean[] answers = new Boolean[questions.length];
        int correct = 0;
        for(int i = 0; i < questions.length; i++) {
          answers[i] = questions[i].getAnswer().equals(responses.get(i));
          if(answers[i]) correct++;
        }

        // Record quiz results in the database
        Document attempt = new Document("user_id", id)
          .append("cid", cid)
          .append("uid", uid)
          .append("pid", qid)
          .append("answers", Arrays.asList(answers))
          .append("responses", responses)
          .append("results", new Document("total", questions.length)
            .append("correct", correct)
            .append("grade", ((double)correct / (double)questions.length))
          );
        quizzes.insertOne(attempt);

        return answers;

      }
    };
  }

  public static DataFetcher getAddUserResolver(MongoClient client) {
    return new DataFetcher<User>() {
      @Override
      public User get(DataFetchingEnvironment env) {
        String id = env.getContext();
        String email = env.getArgument("email");

        User requester = new User(client, id);
        if(!requester.getIsTeacher()) {
          throw new GraphQLException("You do not have permission to add users.");
        }

        MongoCollection users = client
          .getDatabase("elearning")
          .getCollection("users");

        Document user = (Document)users
          .find(eq("email", email))
          .first();

        if(user != null) {
          return null;
        }

        String tempPass = Hashing
          .sha256()
          .hashString(email, StandardCharsets.UTF_8)
          .toString()
          .substring(0, 15);

        users.insertOne(new Document("email", email)
          .append("password", tempPass)
          .append("enrolments", new ArrayList<String>())
          .append("isTeacher", false)
          .append("temp_pass", true)
        );

        user = (Document)users
          .find(eq("email", email))
          .first();

        return new User(user);
      }
    };
  }

  public static DataFetcher getRemoveUserResolver(MongoClient client) {
    return new DataFetcher<Boolean>() {
      @Override
      public Boolean get(DataFetchingEnvironment env) {
        User requester = new User(client, env.getContext());
        if(!requester.getIsTeacher()) {
          throw new GraphQLException("You do not have permission to delete users.");
        }

        String id = env.getArgument("id");
        client.getDatabase("elearning")
          .getCollection("users")
          .deleteOne(
            eq("_id", new ObjectId(id))
          );

        return true;
      }
    };
  }

  public static DataFetcher getSetIsTeacherResolver(MongoClient client) {
    return new DataFetcher<User>() {
      @Override
      public User get(DataFetchingEnvironment env) {
        User requester = new User(client, env.getContext());
        if(!requester.getIsTeacher()) {
          throw new GraphQLException("You do not have permission to set teacher priviledges.");
        }

        String id = env.getArgument("id");
        Boolean state = env.getArgument("status");
        if(requester.getId().equals(id)) {
          throw new GraphQLException("You may not set your own teacher priviledges.");
        }

        MongoCollection users = client
          .getDatabase("elearning")
          .getCollection("users");

        users.updateOne(
          eq("_id", new ObjectId(id)),
          new Document("$set", new Document("isTeacher", state))
        );

        return new User(
          (Document)users.find(eq("_id", new ObjectId(id))).first()
        );
      }
    };
  }

  public static DataFetcher getEnrollCourseResolver(MongoClient client) {
    return new DataFetcher<Boolean>() {
      @Override
      public Boolean get(DataFetchingEnvironment env) {
        User requester = new User(client, env.getContext());
        if(!requester.getIsTeacher()) {
          throw new GraphQLException("You do not have permission to enroll users in courses.");
        }

        String id = env.getArgument("id");
        String cid = env.getArgument("cid");
        User user = new User(client, id);

        Course requestedCourse = new Course(cid);
        if(!requestedCourse.isValid()) {
          throw new GraphQLException ("'" + cid + "' is not a valid course selection.");
        }

        MongoCollection users = client
          .getDatabase("elearning")
          .getCollection("users");

        ArrayList<String> enrolments = new ArrayList<String>(Arrays.asList(user.getEnrolmentsIds()));

        if(!enrolments.contains(cid)) enrolments.add(cid);

        users.updateOne(
          eq("_id", new ObjectId(id)),
          new Document("$set", new Document("enrolments", enrolments))
        );

        return true;
      }
    };
  }

  public static DataFetcher getWithdrawCourseResolver(MongoClient client) {
    return new DataFetcher<Boolean>() {
      @Override
      public Boolean get(DataFetchingEnvironment env) {
        User requester = new User(client, env.getContext());
        if(!requester.getIsTeacher()) {
          throw new GraphQLException("You do not have permission to enroll users in courses.");
        }

        String id = env.getArgument("id");
        String cid = env.getArgument("cid");
        User user = new User(client, id);

        MongoCollection users = client
          .getDatabase("elearning")
          .getCollection("users");

        ArrayList<String> enrolments = new ArrayList<String>(Arrays.asList(user.getEnrolmentsIds()));

        enrolments.remove(cid);

        users.updateOne(
          eq("_id", new ObjectId(id)),
          new Document("$set", new Document("enrolments", enrolments))
        );

        return true;
      }
    };
  }

  public static DataFetcher getPasswordResetResolver(MongoClient client) {
    return new DataFetcher<Boolean>() {
      @Override
      public Boolean get(DataFetchingEnvironment env) {
        String id = env.getArgument("id");
        String password = env.getArgument("password");

        User requester = new User(client, env.getContext());
        if(!requester.getId().equals(id)) {
          throw new GraphQLException("You do not have permission to reset this password.");
        }

        String passHash = Hashing
          .sha256()
          .hashString(
            requester.getId() + requester.getEmail() + password,
            StandardCharsets.UTF_8
          )
          .toString();

        client
          .getDatabase("elearning")
          .getCollection("users")
          .updateOne(
            eq("_id", new ObjectId(id)),
            new Document(
              "$set",
              new Document("password", passHash)
                .append("temp_pass", false)
            )
          );

        return true;
      }
    };
  }
}