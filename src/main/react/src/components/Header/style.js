export default (theme) => ({
  header: {
    alignItems: 'center',
    backgroundColor: theme.color.primary,
    boxShadow: '0px 1px 1px 1px rgba(0, 0, 0, 0.3)',
    color: theme.color.light,
    display: 'flex',
    height: '6rem',
    position: 'relative',
    width: '100%',
    zIndex: 5
  },
  title: {
    fontFamily: theme.font.family.header,
    fontSize: theme.font.size.m,
    fontWeight: 500,
    margin: '0 -6rem 0 auto'
  },
  backlink: {
    color: '#fff',
    fontFamily: theme.font.family.header,
    fontSize: theme.font.size.s,
    margin: `0 -6rem 0 ${theme.spacing.s}`,
    textDecoration: 'none'
  },
  backlink_label: {
    display: 'inline'
  },
  chevron: {
    marginRight: theme.spacing.xs
  },
  '@media(max-width: 425px)': {
    title: {
      fontSize: theme.font.size.s
    }
  },
  '@media(max-width: 768px)': {
    header: {
      position: 'fixed'
    },
    title: {
      textAlign: 'center',
      margin: '0 0 0 auto',
      width: '50%'
    },
    backlink: {
      fontSize: theme.font.size.m,
      margin: 0,
      padding: `0 ${theme.spacing.xs} 0 ${theme.spacing.s}`
    },
    backlink_label: {
      display: 'none'
    }
  }
});
