// React
import React from 'react';
import Fontawesome from 'react-fontawesome';

// React Router
import { Link } from 'react-router-dom';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

export class Profile extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: false
    };
  }

  closeProfile = (e) => {
    this.setState({ expanded: false });
  };

  doLogout = (e) => {
    let d = new Date();
    d.setDate(d.getDate() - 5);

    document.cookie = `jwt=;expires=${d.toUTCString()};`;
    localStorage.removeItem('token');

    window.location.replace('/');
  };

  componentDidMount() {
    addEventListener('click', this.closeProfile);
  }

  render() {
    const { classes, user, ...props } = this.props;

    return (
      <div
        className={`${classes.profile} ${this.state.expanded &&
          classes.expanded}`}
        id="profile"
        onClick={(e) => {
          e.stopPropagation();
          this.setState((prevState) => ({ expanded: !prevState.expanded }));
        }}
      >
        <Fontawesome className={classes.icon} name="user-circle" />
        {this.state.expanded && (
          <div
            id="profile-dropdown"
            className={classes.dropdown}
            onClick={(e) => {
              e.stopPropagation();
            }}
          >
            <h3>{user.email}</h3>
            <ul>
              <li>
                <Link to="/" onClick={this.closeProfile}>
                  <span> </span>
                  Courses
                </Link>
              </li>
              {user.isTeacher && (
                <li>
                  <a href="/admin">
                    <span> </span>
                    Admin Panel
                  </a>
                </li>
              )}
              <li onClick={this.doLogout} style={{ cursor: 'pointer' }}>
                Logout
              </li>
            </ul>
          </div>
        )}
      </div>
    );
  }

  componentWillUnmount() {
    removeEventListener('click', this.closeProfile);
  }
}

export default injectSheet(styles)(Profile);
