import color from 'color';

export default (theme) => ({
  profile: {
    alignItems: 'center',
    cursor: 'pointer',
    display: 'flex',
    fontSize: theme.font.size.m,
    height: '6rem',
    justifyContent: 'center',
    margin: `0 0 0 auto`,
    position: 'relative',
    width: '6rem',
    zIndex: 4
  },
  expanded: {
    backgroundColor: theme.color.secondary,
    borderLeft: `2px solid ${color(theme.color.secondary)
      .darken(0.3)
      .hex()}`
  },
  dropdown: {
    color: theme.color.dark,
    cursor: 'default',
    position: 'absolute',
    right: 0,
    top: '100%',
    width: '100vw',
    zIndex: 2,

    '& h3': {
      backgroundColor: theme.color.secondary,
      color: theme.color.light,
      fontFamily: theme.font.family.header,
      fontSize: theme.font.size.m,
      fontWeight: 'normal',
      margin: 0,
      padding: theme.spacing.s,
      position: 'relative',
      width: '100%',
      zIndex: 3
    },
    '& ul': {
      backgroundColor: theme.color.light,
      boxShadow: '2px 0 1px 2px rgba(0, 0, 0, 0.3)',
      fontFamily: theme.font.family.body,
      fontSize: theme.font.size.s,
      listStyle: 'none',
      margin: 0,
      padding: 0,
      position: 'relative',
      zIndex: 2,

      '& li': {
        borderBottom: `1px solid #eee`,
        padding: theme.spacing.s,
        paddingLeft: theme.spacing.m,
        position: 'relative',

        '& a': {
          color: theme.color.dark,
          cursor: 'pointer',
          textDecoration: 'none',

          '& span': {
            height: '100%',
            left: 0,
            position: 'absolute',
            top: 0,
            width: '100%'
          }
        }
      }
    }
  },

  '@media (min-width: 510px)': {
    dropdown: {
      minWidth: '35rem',
      width: 'auto'
    }
  }
});
