import React from 'react';
import { shallow } from 'enzyme';

import { Profile } from './';

describe('by default', () => {
  const testUser = {
    email: 'john@example.com'
  };

  it('should render the collapsed Profile component correctly', () => {
    const wrapper = shallow(<Profile classes={{}} user={testUser} />);
    expect(wrapper).toMatchSnapshot();

    wrapper.unmount();
  });

  it('should render the expanded Profile component correctly', () => {
    const wrapper = shallow(<Profile classes={{}} user={testUser} />);
    wrapper.find('#profile').simulate('click', {
      stopPropagation: () => {}
    });
    expect(wrapper).toMatchSnapshot();
  });

  it('should collapse the snapshot again', () => {
    const wrapper = shallow(<Profile classes={{}} user={testUser} />);
    expect(wrapper).toMatchSnapshot();

    wrapper.find('#profile').simulate('click', {
      stopPropagation: () => {}
    });

    const name = wrapper.find('#profile h3').text();
    expect(name).toBe(testUser.email);
    wrapper.find('#profile').simulate('click', {
      stopPropagation: () => {}
    });

    expect(wrapper).toMatchSnapshot();
  });

  it('should stop propagation outside of the dropdown', () => {
    const wrapper = shallow(<Profile classes={{}} user={testUser} />);

    const stopPropagation = jest.fn();
    wrapper.find('#profile').simulate('click', {
      stopPropagation: () => {}
    });
    expect(wrapper).toMatchSnapshot();

    const name = wrapper.find('#profile-dropdown').simulate('click', {
      stopPropagation
    });
    expect(wrapper).toMatchSnapshot();
    expect(stopPropagation).toHaveBeenCalledTimes(1);
  });

  it('should close the dropdown from the member callback', () => {
    const wrapper = shallow(<Profile classes={{}} user={testUser} />);
    expect(wrapper).toMatchSnapshot();

    wrapper.find('#profile').simulate('click', {
      stopPropagation: () => {}
    });

    wrapper.instance().closeProfile();

    expect(wrapper).toMatchSnapshot();
  });
});
