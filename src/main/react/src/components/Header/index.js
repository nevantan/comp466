// React
import React from 'react';

// React Router
import { Link } from 'react-router-dom';

// GraphQL
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

// Fontawesome
import Fontawesome from 'react-fontawesome';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

// Components
import Profile from './components/Profile';

export const GET_USER = gql`
  query GetUser($id: ID!) {
    user(id: $id) {
      id
      email
      isTeacher
    }
  }
`;

export const Header = ({ classes, title, parent, userid, ...props }) => (
  <header className={classes.header}>
    {parent.link && (
      <Link to={parent.link} className={classes.backlink}>
        <Fontawesome name="chevron-left" className={classes.chevron} />
        <div className={classes.backlink_label}>{parent.label}</div>
      </Link>
    )}
    <h1 className={classes.title}>{title}</h1>
    <Query query={GET_USER} variables={{ id: userid }}>
      {({ loading, error, data }) => {
        if (loading) return <div className={classes.loading}>Loading...</div>;
        if (error) return <div clssName={classes.error}>{error.message}</div>;
        return <Profile user={data.user} />;
      }}
    </Query>
  </header>
);

export default injectSheet(styles)(Header);
