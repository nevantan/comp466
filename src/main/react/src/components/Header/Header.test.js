import React from 'react';
import { shallow } from 'enzyme';

import { Header } from './';

describe('by default', () => {
  it('renders the top-level Header component correctly', () => {
    const wrapper = shallow(<Header classes={{}} parent={{}} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('renders the child page Header component correctly', () => {
    const wrapper = shallow(
      <Header classes={{}} parent={{ link: '/', label: 'All Courses' }} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
