import React from 'react';
import { shallow } from 'enzyme';

import { CourseCard } from './';

describe('by default', () => {
  it('renders the CourseCard component correctly', () => {
    const testCourse = {
      name: 'Test Course',
      code: 'TEST101',
      description: 'This is a test course.'
    };

    const wrapper = shallow(
      <CourseCard
        classes={{ card: 'card', title: 'title', description: 'description' }}
        course={testCourse}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
