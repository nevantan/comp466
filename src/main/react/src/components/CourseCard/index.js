// React
import React from 'react';

// React Router
import { Link } from 'react-router-dom';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

export const CourseCard = ({ classes, course, ...props }) => (
  <div className={classes.card}>
    <h2 className={classes.title}>
      <small>{course.code}</small>
      <br />
      <Link to={`/course/${course.id}`}>
        {course.title}
        <span> </span>
      </Link>
    </h2>
    <div className={classes.description}>{course.description}</div>
  </div>
);

export default injectSheet(styles)(CourseCard);
