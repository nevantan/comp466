import color from 'color';

export default (theme) => ({
  card: {
    borderRadius: '0.2rem',
    boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.2)',
    flex: '0 0 100%',
    margin: `0 ${theme.spacing.xs} ${theme.spacing.xs} 0`,
    padding: theme.spacing.s,
    position: 'relative'
  },
  title: {
    fontFamily: theme.font.family.header,
    fontSize: theme.font.size.s,
    fontWeight: 'normal',
    margin: `0 0 ${theme.spacing.xs} 0`,

    '& small': {
      color: color(theme.color.dark)
        .lighten(2.5)
        .whiten(0.5)
        .hex(),
      fontSize: '60%'
    },

    '& a': {
      color: theme.color.dark,
      textDecoration: 'none',

      '& span': {
        height: '100%',
        left: 0,
        position: 'absolute',
        top: 0,
        width: '100%'
      }
    }
  },
  description: {
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.xs
  },

  '@media (min-width: 590px)': {
    card: {
      flex: '0 0 49.1%',

      '&:nth-child(2n)': {
        marginRight: 0
      }
    }
  },

  '@media (min-width: 860px)': {
    card: {
      flex: '0 0 32.5%',

      '&:nth-child(2n)': {
        marginRight: theme.spacing.xs
      },

      '&:nth-child(3n)': {
        marginRight: 0
      }
    }
  }
});
