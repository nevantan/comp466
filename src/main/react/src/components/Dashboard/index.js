// React
import React from 'react';

// React Router
import {
  BrowserRouter as Router,
  Route,
  Redirect,
  Link
} from 'react-router-dom';

// GraphQL
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

// Pages
import AdminPanel from '../pages/AdminPanel';
import AllCoursesPage from '../pages/AllCourses';
import CoursePage from '../pages/Course';
import ResetPasswordPage from '../pages/ResetPassword';

// Components
import Header from '../Header';

export const GET_USER = gql`
  query GetUser($id: ID!) {
    user(id: $id) {
      id
      email
      usingTempPass
    }
  }
`;

export class Dashboard extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      title: 'All Courses',
      parent: {}
    };
  }

  setTitle = (title) => {
    this.setState({ title });
  };

  setParent = (parent) => {
    this.setState({ parent });
  };

  allCourses = (props) => (
    <AllCoursesPage
      {...props}
      title={this.state.title}
      setTitle={this.setTitle}
      setParent={this.setParent}
      graphql={this.props.graphql}
    />
  );

  courseDetails = (props) => (
    <CoursePage
      {...props}
      title={this.state.title}
      setTitle={this.setTitle}
      setParent={this.setParent}
      graphql={this.props.graphql}
    />
  );

  adminPanel = (data) => (props) => (
    <AdminPanel
      {...props}
      title={this.state.title}
      setTitle={this.setTitle}
      setParent={this.setParent}
      graphql={this.props.graphql}
      userid={this.props.userid}
    />
  );

  resetPassword = (data) => (props) => (
    <ResetPasswordPage
      {...props}
      title={this.state.title}
      setTitle={this.setTitle}
      setParent={this.setParent}
      graphql={this.props.graphql}
      userid={this.props.userid}
    />
  );

  page = (Component, data) => (props) => (
    <Component
      {...props}
      title={this.state.title}
      setTitle={this.setTitle}
      setParent={this.setParent}
      graphql={this.props.graphql}
      userid={this.props.userid}
    />
  );

  render() {
    const { classes, redirect, userid, ...props } = this.props;
    return (
      <div className={classes.dashboard}>
        <Router>
          <React.Fragment>
            {redirect && <Redirect to="/" />}
            <Query query={GET_USER} variables={{ id: userid }}>
              {({ loading, error, data }) => {
                if (loading)
                  return <div className={classes.loading}>Loading...</div>;
                if (error)
                  return <div className={classes.error}>{error.message}</div>;

                return (
                  <React.Fragment>
                    {data.user.usingTempPass && <Redirect to="/reset" />}

                    <Header
                      title={this.state.title}
                      parent={this.state.parent}
                      userid={userid}
                    />

                    <Route
                      exact
                      id="all_courses"
                      path="/"
                      render={this.allCourses}
                    />

                    <Route
                      id="reset_password"
                      path="/reset"
                      render={this.page(ResetPasswordPage, data)}
                    />

                    <Route
                      id="course_details"
                      path="/course/:course_id"
                      render={this.courseDetails}
                    />

                    <Route
                      id="admin_panel"
                      path="/admin"
                      render={this.adminPanel(data)}
                    />
                  </React.Fragment>
                );
              }}
            </Query>
          </React.Fragment>
        </Router>
      </div>
    );
  }
}

export default injectSheet(styles)(Dashboard);
