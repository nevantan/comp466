import React from 'react';
import { Route, MemoryRouter } from 'react-router-dom';
import { shallow, mount } from 'enzyme';

import { Dashboard } from './';

describe('by default', () => {
  it('renders the Dashboard component correctly', () => {
    const wrapper = shallow(
      <Dashboard classes={{ dashboard: '.dashboard' }} redirect={false} />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('redirects if the appropriate prop is set', () => {
    const wrapper = shallow(<Dashboard classes={{}} redirect={true} />);
    expect(wrapper).toMatchSnapshot();
  });
});

describe('setTitle', () => {
  it('sets the Title that is then passed to the header', () => {
    const wrapper = shallow(<Dashboard classes={{}} redirect={false} />);

    const titleText = 'Hello World!';
    wrapper.instance().setTitle(titleText);
    expect(wrapper.state('title')).toBe(titleText);
  });
});

describe('setParent', () => {
  it('sets the Parent that is then passed to the header', () => {
    const wrapper = shallow(<Dashboard classes={{}} redirect={false} />);

    const parentObj = {};
    wrapper.instance().setParent(parentObj);
    expect(wrapper.state('parent')).toBe(parentObj);
  });
});

describe('the route render methods', () => {
  it('return the correct All Courses route object', () => {
    const wrapper = shallow(<Dashboard classes={{}} redirect={false} />);

    const route = wrapper.instance().allCourses();
    expect(route).toMatchSnapshot();
  });

  it('return the correct Course Details route object', () => {
    const wrapper = shallow(<Dashboard classes={{}} redirect={false} />);

    const route = wrapper.instance().courseDetails();
    expect(route).toMatchSnapshot();
  });
});
