export default (theme) => ({
  '@global': {
    '*': {
      boxSizing: 'border-box'
    },
    'html, body': {
      backgroundColor: theme.color.light,
      color: theme.color.dark,
      margin: 0,
      maxWidth: '100vw',
      padding: 0
    },
    html: {
      fontSize: '62.5%'
    }
  }
});
