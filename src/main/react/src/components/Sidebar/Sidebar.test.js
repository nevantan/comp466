import React from 'react';
import { shallow } from 'enzyme';

import { Sidebar } from './';

describe('by default', () => {
  it('renders the Sidebar component correctly', () => {
    const wrapper = shallow(<Sidebar classes={{}} />);
    expect(wrapper).toMatchSnapshot();
  });
});
