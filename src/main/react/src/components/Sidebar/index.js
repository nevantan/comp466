// React
import React from 'react';
import Fontawesome from 'react-fontawesome';
// Styles
import injectSheet from 'react-jss';
import styles from './style';

export class Sidebar extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { classes, children, ...props } = this.props;
    return (
      <div
        className={`${classes.sidebar} ${
          this.props.expanded ? classes.sidebar_open : classes.sidebar_closed
        }`}
      >
        <div className={classes.children}>{children}</div>
        <div className={classes.toggle} onClick={this.props.toggle}>
          <Fontawesome name={this.props.expanded ? 'times' : 'bars'} />
        </div>
      </div>
    );
  }
}

export default injectSheet(styles)(Sidebar);
