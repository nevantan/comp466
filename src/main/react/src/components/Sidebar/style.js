import color from 'color';

export default (theme) => ({
  sidebar: {
    backgroundColor: theme.color.secondary,
    boxShadow: '0px -1px 1px 1px rgba(0, 0, 0, 0.3) inset',
    color: theme.color.light,
    flex: '0 0 25rem',
    fontFamily: theme.font.family.header,
    fontSize: theme.font.size.s,
    minHeight: 'calc(100vh - 6rem)',

    '& ul': {
      listStyle: 'none',
      margin: 0,
      padding: 0,

      '& li': {
        position: 'relative',

        '& a': {
          color: theme.color.light,
          textDecoration: 'none',

          '& span': {
            height: '100%',
            left: 0,
            position: 'absolute',
            top: 0,
            width: '100%'
          }
        },

        '& ul': {
          fontSize: theme.font.size.xs,

          '& li': {
            backgroundColor: 'rgba(255, 255, 255, 0.1)',
            padding: `${theme.spacing.xs} ${theme.spacing.l}`
          }
        }
      }
    }
  },
  toggle: {
    display: 'none'
  },
  '@media(max-width: 768px)': {
    sidebar: {
      position: 'fixed',
      top: '6rem',
      transition: 'margin 200ms ease-in-out',
      width: '25rem'
    },
    sidebar_open: {
      margin: '0 0 0 0'
    },
    sidebar_closed: {
      margin: '0 0 0 -25rem'
    },
    toggle: {
      alignItems: 'center',
      backgroundColor: theme.color.primary,
      borderRadius: '50%',
      bottom: theme.spacing.s,
      boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.3)',
      color: theme.color.light,
      display: 'flex',
      fontSize: theme.font.size.m,
      height: '5rem',
      justifyContent: 'center',
      left: theme.spacing.s,
      position: 'fixed',
      width: '5rem',
      zIndex: 100
    }
  }
});
