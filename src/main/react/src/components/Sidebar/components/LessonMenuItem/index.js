// React
import React from 'react';
import PropTypes from 'prop-types';

// React Router
import { Link } from 'react-router-dom';

const LessonMenuItem = ({ cid, uid, lesson, onClick, ...props }) => (
  <li>
    <Link to={`/course/${cid}/${uid}/l/${lesson.id}`} onClick={onClick}>
      <span> </span>
      {lesson.title}
    </Link>
  </li>
);

export default LessonMenuItem;

LessonMenuItem.propTypes = {
  cid: PropTypes.string.isRequired,
  uid: PropTypes.string.isRequired,
  lesson: PropTypes.object.isRequired
};
