import React from 'react';
import { shallow } from 'enzyme';

import LessonMenuItem from './';

describe('by default', () => {
  it('renders the LessonMenuItem component correctly', () => {
    const cid = 'course1';
    const uid = 'unit1';
    const lesson = {
      id: 'lesson1',
      title: 'Test Lesson 1'
    };

    const wrapper = shallow(
      <LessonMenuItem cid={cid} uid={uid} lesson={lesson} />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
