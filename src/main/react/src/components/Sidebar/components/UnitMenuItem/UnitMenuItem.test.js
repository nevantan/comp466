import React from 'react';
import { shallow } from 'enzyme';

import UnitMenuItem from './';

describe('by default', () => {
  const cid = 'course1';
  const unit = {
    id: 'unit1',
    title: 'Test Unit 1',
    pages: [
      { id: 'lesson1', title: 'Test Lesson 1', type: 'lesson' },
      { id: 'quiz1', title: 'Test Quiz 1', type: 'quiz' },
      { id: 'foo1', title: 'Foo 1', type: 'foo' }
    ]
  };

  it('renders the collapsed UnitMenuItem component correctly', () => {
    const wrapper = shallow(
      <UnitMenuItem
        classes={{}}
        cid={cid}
        unit={unit}
        expanded={false}
        onClick={() => {}}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('renders the expanded UnitMenuItem component correctly', () => {
    const wrapper = shallow(
      <UnitMenuItem
        classes={{}}
        cid={cid}
        unit={unit}
        expanded={true}
        onClick={() => {}}
      />
    );
    expect(wrapper).toMatchSnapshot();
  });
});
