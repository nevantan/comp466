// React
import React from 'react';
import PropTypes from 'prop-types';

// Fontawesome
import Fontawesome from 'react-fontawesome';

import LessonMenuItem from '../LessonMenuItem';
import QuizMenuItem from '../QuizMenuItem';

const UnitMenuItem = ({
  classes,
  cid,
  onClick,
  closeSidebar,
  unit,
  expanded,
  ...props
}) => (
  <li id={unit.id}>
    <div
      className={`${classes.unit} ${classes.unit_expanded}`}
      onClick={onClick}
    >
      {unit.title}
      <Fontawesome name={expanded ? 'minus' : 'chevron-down'} />
    </div>
    {expanded && (
      <ul>
        {unit.pages.map((page, i) => (
          <React.Fragment key={i}>
            {(() => {
              switch (page.type) {
                case 'lesson':
                  return (
                    <LessonMenuItem
                      className={classes.page}
                      cid={cid}
                      uid={unit.id}
                      lesson={page}
                      onClick={closeSidebar}
                    />
                  );
                case 'quiz':
                  return (
                    <QuizMenuItem
                      className={classes.page}
                      cid={cid}
                      uid={unit.id}
                      quiz={page}
                      onClick={closeSidebar}
                    />
                  );
                default:
                  return <div>Error</div>;
              }
            })()}
          </React.Fragment>
        ))}
      </ul>
    )}
  </li>
);

export default UnitMenuItem;

UnitMenuItem.propTypes = {
  classes: PropTypes.object.isRequired,
  cid: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
  unit: PropTypes.object.isRequired,
  expanded: PropTypes.bool.isRequired
};
