// React
import React from 'react';
import PropTypes from 'prop-types';

// React Router
import { Link } from 'react-router-dom';

const QuizMenuItem = ({ cid, uid, quiz, onClick, ...props }) => (
  <li>
    <Link to={`/course/${cid}/${uid}/q/${quiz.id}`} onClick={onClick}>
      <span> </span>
      {quiz.title}
    </Link>
  </li>
);

export default QuizMenuItem;

QuizMenuItem.propTypes = {
  cid: PropTypes.string.isRequired,
  uid: PropTypes.string.isRequired,
  quiz: PropTypes.object.isRequired
};
