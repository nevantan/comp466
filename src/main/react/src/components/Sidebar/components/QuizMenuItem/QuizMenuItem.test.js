import React from 'react';
import { shallow } from 'enzyme';

import QuizMenuItem from './';

describe('by default', () => {
  it('renders the QuizMenuItem component correctly', () => {
    const cid = 'course1';
    const uid = 'unit1';
    const quiz = {
      id: 'quiz1',
      title: 'Test Quiz 1'
    };

    const wrapper = shallow(<QuizMenuItem cid={cid} uid={uid} quiz={quiz} />);
    expect(wrapper).toMatchSnapshot();
  });
});
