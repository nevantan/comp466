// React
import React from 'react';

// GraphQL
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style.js';

// Components
import CourseCard from '../../CourseCard';

export const GET_COURSES = gql`
  query AuthenticatedUser($user_id: ID!) {
    user(id: $user_id) {
      id
      enrolments {
        id
        code
        title
        description
      }
    }
  }
`;

export class AllCourses extends React.Component {
  constructor(props) {
    super(props);

    props.setTitle('All Courses');
    props.setParent({});

    this.state = {
      user_id: null
    };
  }

  componentDidMount() {
    this.props.graphql
      .query({
        query: gql`
          {
            user_id @client
          }
        `
      })
      .then((result) => {
        this.setState({
          user_id: result.data.user_id
        });
      });
  }

  render() {
    const { classes, ...props } = this.props;
    return (
      <div className={classes.courses}>
        {this.state.user_id ? (
          <Query
            query={GET_COURSES}
            variables={{ user_id: this.state.user_id }}
          >
            {({ loading, error, data }) => {
              if (loading)
                return (
                  <div className={classes.loading}>Loading courses...</div>
                );
              if (error)
                return <div className={classes.error}>{error.message}</div>;

              return data.user.enrolments.map((course, i) => (
                <CourseCard course={course} key={i} />
              ));
            }}
          </Query>
        ) : (
          <div>Loading...</div>
        )}
      </div>
    );
  }
}

export default injectSheet(styles)(AllCourses);
