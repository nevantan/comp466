Feature: All Courses page

  Background:
    Given I am logged in
    And I am on the All Courses page

  Scenario: I want to access one of my courses
    When I click on the course card
    Then I should be on the Course Details page