import React from 'react';
import { shallow, mount } from 'enzyme';

// GraphQL
import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';
import { GET_COURSES, AllCourses } from './';

// Styles
import { ThemeProvider } from 'react-jss';
import theme from '../../../styles/theme';

// Router
import { StaticRouter } from 'react-router-dom';

const result = {
  data: {
    user: {
      id: 'foobar',
      enrolments: [
        {
          id: 'foo',
          code: 'FOO101',
          title: 'Test Title',
          description: 'A test course description.'
        }
      ]
    }
  }
};

const mocks = [
  {
    request: {
      query: GET_COURSES,
      variables: { user_id: 'foobar' }
    },
    result: {
      ...result
    }
  },
  {
    request: {
      query: GET_COURSES,
      variables: { user_id: 'foobarbaz' }
    },
    result: {
      ...result
    }
  }
];

describe('by default', () => {
  it('renders the AllCoursesPage component correctly', () => {
    const setTitle = jest.fn();
    const setParent = jest.fn();
    const graphql = {
      query: jest.fn()
    };
    graphql.query.mockReturnValue(
      new Promise((resolve, reject) => {
        resolve({
          data: {
            user_id: 'foobar'
          }
        });
      })
    );

    const wrapper = shallow(
      <AllCourses
        setTitle={setTitle}
        setParent={setParent}
        graphql={graphql}
        classes={{}}
      />
    );
    expect(wrapper).toMatchSnapshot();

    expect(setTitle).toHaveBeenCalledTimes(1);
    expect(setTitle).toHaveBeenCalledWith('All Courses');

    expect(setParent).toHaveBeenCalledTimes(1);
    expect(setParent).toHaveBeenCalledWith({});
  });

  it('fetches data from a GraphQL endpoint correctly and shows the loading state', () => {
    const graphql = {
      query: jest.fn()
    };
    graphql.query.mockReturnValue(
      new Promise((resolve, reject) => {
        resolve({
          data: {
            user_id: 'foobar'
          }
        });
      })
    );

    const wrapper = mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <AllCourses
              setTitle={() => {}}
              setParent={() => {}}
              graphql={graphql}
              classes={{ loading: 'loading', error: 'error' }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    wrapper
      .find(AllCourses)
      .instance()
      .setState({ user_id: 'foobar' });
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(1);
    expect(wrapper.find('.loading').text()).toBe('Loading courses...');
  });

  it('fetches data from a GraphQL endpoint correctly and shows the final state', async () => {
    const graphql = {
      query: jest.fn()
    };
    graphql.query.mockReturnValue(
      new Promise((resolve, reject) => {
        resolve({
          data: {
            user_id: 'foobarbaz'
          }
        });
      })
    );

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <AllCourses
              setTitle={() => {}}
              setParent={() => {}}
              graphql={graphql}
              classes={{ loading: 'loading', error: 'error' }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);

    wrapper.update();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(0);
  });

  it('fetches data from a GraphQL endpoint correctly and shows the error state', async () => {
    const errorMocks = [
      {
        request: {
          query: GET_COURSES,
          variables: { user_id: 'foobar' }
        },
        error: new Error('This is a test error.')
      }
    ];
    const graphql = {
      query: jest.fn()
    };
    graphql.query.mockReturnValue(
      new Promise((resolve, reject) => {
        resolve({
          data: {
            user_id: 'foobar'
          }
        });
      })
    );

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={errorMocks} addTypename={false}>
            <AllCourses
              setTitle={() => {}}
              setParent={() => {}}
              graphql={graphql}
              classes={{ loading: 'loading', error: 'error' }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);

    wrapper.update();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(1);
    expect(wrapper.find('.error').text()).toBe(
      'Network error: This is a test error.'
    );
  });
});
