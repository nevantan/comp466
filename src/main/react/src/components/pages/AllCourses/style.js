export default (theme) => ({
  courses: {
    display: 'flex',
    flexWrap: 'wrap',
    margin: '0 auto',
    maxWidth: '100rem',
    padding: theme.spacing.s,
    width: '100%'
  },
  '@media(max-width: 768px)': {
    courses: {
      padding: `7.6rem ${theme.spacing.s} ${theme.spacing.s} ${theme.spacing.s}`
    }
  }
});
