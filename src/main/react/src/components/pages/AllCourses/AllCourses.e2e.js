import puppeteer from 'puppeteer';

describe('the All Courses page', () => {
  let browser;
  let page;

  beforeEach(async () => {
    // Setup puppeteer
    browser = await puppeteer.launch({
      headless: true
    });
    page = await browser.newPage();

    page.emulate({
      viewport: {
        width: 800,
        height: 600
      },
      userAgent: ''
    });

    // Goto homepage and wait for title
    await page.goto('http://127.0.0.1:8080/');
    await page.waitForSelector('.card h2');

    // Make sure we are on the login page
    let title = await page.$eval('.card h2', (e) => e.innerText);
    expect(title).toBe('Login');

    // Given I am logged in
    await page.type('#email', process.env.EMAIL);
    await page.type('#password', process.env.PASSWORD);
    await page.click('[type="submit"]');

    await page.waitForSelector('#app h1');

    title = await page.$eval('#app h1', (e) => e.innerText);
    expect(title).toBe('All Courses');
  }, 10000);

  it(
    'allows me to access one of my courses',
    async () => {
      const el = await page.$x(
        "//a[contains(text(), 'Introduction to JavaScript')]"
      );
      await el[0].click();

      await page.waitForSelector('#app h1');

      const title = await page.$eval('#app h1', (e) => e.innerText);
      expect(title).toBe('Introduction to JavaScript');
    },
    10000
  );

  afterEach(async () => {
    // Close browser
    await browser.close();
  }, 10000);
});
