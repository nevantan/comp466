// React
import React from 'react';

// React Router
import { Redirect } from 'react-router-dom';

// GraphQL
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

// Components
import UserList from './components/UserList';

export const IS_ADMIN = gql`
  query IsAdmin($uid: ID!) {
    user(id: $uid) {
      id
      isTeacher
    }
  }
`;

export class AdminPanel extends React.Component {
  constructor(props) {
    super(props);

    props.setTitle('Admin Panel');

    this.state = {
      refetch: false
    };
  }

  render() {
    const { classes, userid, ...props } = this.props;
    return (
      <div className={classes.content}>
        <Query query={IS_ADMIN} variables={{ uid: userid }}>
          {({ loading, error, data }) => {
            if (loading)
              return <div className={classes.loading}>Loading...</div>;
            if (error)
              return <div className={classes.error}>{error.message}</div>;

            if (data.user.isTeacher) {
              return (
                <UserList
                  refetchRequested={this.state.refetch}
                  setRefetchCallback={this.setRefetch}
                  userid={userid}
                />
              );
            } else {
              return <Redirect to="/" />;
            }
          }}
        </Query>
      </div>
    );
  }
}

export default injectSheet(styles)(AdminPanel);
