// React
import React from 'react';

// React Modal
import Modal from 'react-modal';

// Fontawesome
import Fontawesome from 'react-fontawesome';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

const ENROLMENTS = gql`
  query Enrolments($user: ID!) {
    user(id: $user) {
      id
      enrolments {
        id
        code
      }
    }
  }
`;

const ENROLL_COURSE = gql`
  mutation EnrollCourse($user: ID!, $course: ID!) {
    enrollCourse(id: $user, cid: $course)
  }
`;

const WITHDRAW_COURSE = gql`
  mutation WithdrawCourse($user: ID!, $course: ID!) {
    withdrawCourse(id: $user, cid: $course)
  }
`;

export class AddCourse extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      isOpen: false,
      course: ''
    };
  }

  closeModal = () => {
    this.setState(
      {
        isOpen: false
      },
      () => {
        this.props.refetch();
      }
    );
  };

  openModal = () => {
    this.setState({
      isOpen: true
    });
  };

  onCourseChange = (e) => {
    this.setState({
      course: e.target.value
    });
  };

  doEnroll = (enrollCourse) => (e) => {
    enrollCourse({
      variables: {
        user: this.props.user.id,
        course: this.state.course.toLowerCase()
      }
    });
  };

  doWithdraw = (withdrawCourse) => (e) => {
    withdrawCourse({
      variables: {
        user: this.props.user.id,
        course: e.target.id
      }
    });
  };

  render() {
    const { classes, user, refetch, ...props } = this.props;
    return (
      <div className={classes.wrap}>
        {user.enrolments.map((course, i) => (
          <span className={classes.pill} key={i}>
            {course.code}
          </span>
        ))}
        <span onClick={this.openModal} className={`${classes.pill} primary`}>
          Edit
        </span>

        <Modal
          isOpen={this.state.isOpen}
          onRequestClose={this.closeModal}
          className={classes.modal}
          overlayClassName={classes.overlay}
          contentLabel="Add User Modal"
          appElement={document.getElementById('app')}
        >
          <button onClick={this.closeModal} className={classes.btnClose}>
            <Fontawesome name="times" />
          </button>

          <h2>Add/Remove Courses</h2>

          <Query query={ENROLMENTS} variables={{ user: user.id }}>
            {({ loading, error, data, refetch }) => {
              if (loading)
                return <div className={classes.loading}>Loading...</div>;
              if (error) {
                return <div className={classes.error}>{error.message}</div>;
              }

              return (
                <React.Fragment>
                  <div className={classes.inputWrap}>
                    <input
                      type="text"
                      className={classes.addCourse}
                      placeholder="Begin typing course code..."
                      list="courses"
                      value={this.state.course}
                      onChange={this.onCourseChange}
                    />
                    <datalist id="courses">
                      <option value="ECMA200" />
                    </datalist>

                    <Mutation
                      mutation={ENROLL_COURSE}
                      errorPolicy="ignore"
                      onCompleted={() => {
                        this.setState(
                          {
                            course: ''
                          },
                          () => {
                            refetch();
                          }
                        );
                      }}
                      onError={({ response, operation }) => {
                        alert('Please select a valid course');
                      }}
                    >
                      {(enrollCourse, { loading, error, data }) => {
                        if (loading)
                          return (
                            <div className={classes.loading}>Loading...</div>
                          );

                        return (
                          <button
                            onClick={this.doEnroll(enrollCourse)}
                            className={classes.btnPrimary}
                          >
                            Add Course
                          </button>
                        );
                      }}
                    </Mutation>
                  </div>
                  <h3>Courses (click to remove):</h3>
                  <hr />
                  <div className={classes.courseList}>
                    <Mutation mutation={WITHDRAW_COURSE} onCompleted={refetch}>
                      {(withdrawCourse, { loading, error, _ }) => {
                        if (loading)
                          return (
                            <div className={classes.loading}>Loading...</div>
                          );
                        if (error)
                          return (
                            <div className={classes.error}>{error.message}</div>
                          );

                        return (
                          <React.Fragment>
                            {data.user.enrolments.map((course, i) => (
                              <div
                                id={course.code.toLowerCase()}
                                onClick={this.doWithdraw(withdrawCourse)}
                                className={classes.pill}
                                key={i}
                              >
                                {course.code}
                              </div>
                            ))}
                          </React.Fragment>
                        );
                      }}
                    </Mutation>
                  </div>
                </React.Fragment>
              );
            }}
          </Query>
        </Modal>
      </div>
    );
  }
}

export default injectSheet(styles)(AddCourse);
