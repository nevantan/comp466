export default (theme) => ({
  ...theme.common,
  modal: {
    ...theme.common.modal,
    marginLeft: '-20rem',
    width: '40rem',

    '& hr': {
      border: 0,
      borderBottom: '1px solid #ccc',
      margin: `0 0 ${theme.spacing.xs} 0`
    },

    '& h3': {
      fontFamily: theme.font.family.heading,
      fontSize: theme.font.size.s,
      fontWeight: 'normal',
      margin: `${theme.spacing.s} 0 0 0`
    }
  },
  btnPrimary: {
    ...theme.common.btnPrimary,
    marginLeft: theme.spacing.xs
  },
  wrap: {
    display: 'flex'
  },
  pill: {
    backgroundColor: theme.color.secondary,
    borderRadius: '20rem',
    color: theme.color.light,
    marginRight: theme.spacing.xs,
    padding: `0.5rem ${theme.spacing.xs}`,

    '&.primary': {
      backgroundColor: 'transparent',
      border: `1px solid ${theme.color.secondary}`,
      color: theme.color.secondary,
      cursor: 'pointer'
    }
  },
  inputWrap: {
    display: 'flex'
  },
  addCourse: {
    flex: 1,
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.xs,
    padding: theme.spacing.xs
  },
  courseList: {
    cursor: 'pointer',
    display: 'flex',
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.xs
  }
});
