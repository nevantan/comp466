// React
import React from 'react';

// React Modal
import Modal from 'react-modal';

// Fontawesome
import Fontawesome from 'react-fontawesome';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

export const ADD_USER = gql`
  mutation AddUser($email: String!) {
    addUser(email: $email) {
      id
      email
      password
    }
  }
`;

export class AddUser extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      email: ''
    };
  }

  setEmail = (e) => {
    this.setState({
      email: e.target.value
    });
  };

  render() {
    const { classes, isOpen, requestClose, refetch, ...props } = this.props;
    const close = () => {
      this.setState({ email: '' });
      requestClose();
    };

    return (
      <Modal
        isOpen={isOpen}
        onRequestClose={close}
        className={classes.modal}
        overlayClassName={classes.overlay}
        contentLabel="Add User Modal"
        appElement={document.getElementById('app')}
      >
        <h2>Add User</h2>
        <button className={classes.btnClose} onClick={close}>
          <Fontawesome name="times" />
        </button>

        <Mutation mutation={ADD_USER}>
          {(addUser, { loading, error, data }) => {
            if (loading)
              return <div className={classes.loading}>Loading...</div>;
            if (error)
              return <div className={classes.error}>{error.message}</div>;
            if (data && !data.addUser) {
              return (
                <React.Fragment>
                  <div className={classes.error}>
                    A user with this email address already exists.
                  </div>
                  <button className={classes.btnPrimary} onClick={close}>
                    Close
                  </button>
                </React.Fragment>
              );
            }
            if (data) {
              return (
                <React.Fragment>
                  <div className={classes.success}>
                    User successfully added!
                  </div>
                  <button
                    className={classes.btnPrimary}
                    onClick={() => {
                      refetch();
                      close();
                    }}
                  >
                    Close
                  </button>
                </React.Fragment>
              );
            }

            return (
              <form
                className={classes.addUserForm}
                onSubmit={() =>
                  addUser({
                    variables: {
                      email: this.state.email
                    }
                  })
                }
              >
                <label htmlFor="email">Email</label>
                <input
                  type="email"
                  id="email"
                  placeholder="user@domain.com"
                  value={this.state.email}
                  onChange={this.setEmail}
                />
                <button className={classes.btnPrimary}>Add User</button>
              </form>
            );
          }}
        </Mutation>
      </Modal>
    );
  }
}

export default injectSheet(styles)(AddUser);
