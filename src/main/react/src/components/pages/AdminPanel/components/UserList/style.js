export default (theme) => ({
  ...theme.common,
  tableWrap: {
    overflowX: 'auto',
    width: '100%'
  },
  table: {
    borderCollapse: 'collapse',
    margin: `0 0 ${theme.spacing.s} 0`,
    width: '100%',

    '& th': {
      backgroundColor: theme.color.primary,
      border: `1px solid ${theme.color.primary}`,
      color: theme.color.light,
      fontFamily: theme.font.family.header,
      fontSize: theme.font.size.xs,
      padding: theme.spacing.xs
    }
  }
});
