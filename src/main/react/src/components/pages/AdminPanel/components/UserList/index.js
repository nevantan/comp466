// React
import React from 'react';

// GraphQL
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

// Components
import AddUser from './components/AddUser';
import UserRow from './components/UserRow';

export const GET_USERS = gql`
  query GetAllUsers {
    users {
      id
      email
      password
      isTeacher
      enrolments {
        id
        code
      }
    }
  }
`;

export class UserList extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      addUserOpen: false
    };
  }

  openAddUser = () => {
    this.setState({ addUserOpen: true });
  };

  closeAddUser = () => {
    this.setState({ addUserOpen: false });
  };

  setAddUserEmail = (e) => {
    this.setState({
      addUserEmail: e.target.value
    });
  };

  render() {
    const { classes, users, userid, setRefetchCallback, ...props } = this.props;
    return (
      <div className={classes.list}>
        <Query query={GET_USERS}>
          {({ loading, error, data, refetch }) => {
            if (loading)
              return <div className={classes.loading}>Loading...</div>;
            if (error)
              return <div className={classes.error}>{error.message}</div>;

            return (
              <React.Fragment>
                <div className={classes.tableWrap}>
                  <table className={classes.table}>
                    <thead>
                      <tr>
                        <th>Email</th>
                        <th>Password</th>
                        <th>Courses</th>
                        <th>Teacher</th>
                        <th>Delete</th>
                      </tr>
                    </thead>
                    <tbody>
                      {data.users.map((user, i) => (
                        <UserRow
                          key={i}
                          user={user}
                          refetch={refetch}
                          self={user.id === userid}
                        />
                      ))}
                    </tbody>
                  </table>
                </div>

                <AddUser
                  isOpen={this.state.addUserOpen}
                  requestClose={this.closeAddUser}
                  refetch={refetch}
                />
              </React.Fragment>
            );
          }}
        </Query>

        <div className={classes.controls}>
          <button className={classes.btnPrimary} onClick={this.openAddUser}>
            Add User
          </button>
        </div>
      </div>
    );
  }
}

export default injectSheet(styles)(UserList);
