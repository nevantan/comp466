export default (theme) => ({
  row: {
    '& td': {
      border: `1px solid ${theme.color.dark}`,
      fontFamily: theme.font.family.body,
      fontSize: theme.font.size.xs,
      padding: theme.spacing.xs
    }
  },
  check: {
    position: 'relative',
    textAlign: 'center',

    '& label span': {
      height: '100%',
      left: 0,
      position: 'absolute',
      top: 0,
      width: '100%'
    }
  },
  danger: {
    backgroundColor: 'transparent',
    border: 0,
    color: theme.color.fail,
    height: '100%',
    width: '100%'
  }
});
