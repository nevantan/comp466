// React
import React from 'react';

// Fontawesome
import Fontawesome from 'react-fontawesome';

// GraphQL
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

// Components
import AddCourse from '../AddCourse';

export const DELETE_USER = gql`
  mutation DeleteUser($id: ID!) {
    removeUser(id: $id)
  }
`;

export const SET_TEACHER = gql`
  mutation SetTeacher($id: ID!, $status: Boolean!) {
    setIsTeacher(id: $id, status: $status) {
      id
      isTeacher
    }
  }
`;

export class UserRow extends React.Component {
  constructor(props) {
    super(props);

    this.state = {};
  }

  render() {
    const { classes, user, self, refetch, ...props } = this.props;
    return (
      <tr className={classes.row}>
        <td className={classes.col}>{user.email}</td>
        <td className={classes.col}>{user.password}</td>
        <td className={classes.col}>
          <AddCourse user={user} refetch={refetch} />
        </td>
        <td className={classes.check}>
          <label htmlFor={`${user.id}-teacher`}>
            <span> </span>
            <Mutation
              mutation={SET_TEACHER}
              onCompleted={() => {
                refetch();
              }}
            >
              {(setIsTeacher, { loading, error, data }) => {
                if (loading)
                  return <div className={classes.loading}>Loading...</div>;
                if (error)
                  return <div className={classes.error}>{error.message}</div>;

                return (
                  <input
                    id={`${user.id}-teacher`}
                    type="checkbox"
                    checked={user.isTeacher}
                    disabled={self || false}
                    onChange={(e) => {
                      const message = `Setting ${
                        user.email
                      } as a teacher will give them the same power as your own account, which they may then use to remove your teacher priviledges. Are you sure you want to continue?`;
                      if (!e.target.checked || confirm(message)) {
                        setIsTeacher({
                          variables: {
                            id: user.id,
                            status: e.target.checked
                          }
                        });
                      }
                    }}
                  />
                );
              }}
            </Mutation>
          </label>
        </td>
        <td className={classes.col}>
          <Mutation
            mutation={DELETE_USER}
            onCompleted={() => {
              refetch();
            }}
          >
            {(removeUser, { loading, error, data }) => {
              if (loading)
                return <div className={classes.loading}>Loading...</div>;
              if (error)
                return <div className={classes.error}>{error.message}</div>;

              return (
                <button
                  className={classes.danger}
                  onClick={() => {
                    if (
                      window.confirm(
                        `Are you sure you want to delete ${user.email}?`
                      )
                    ) {
                      removeUser({
                        variables: {
                          id: user.id
                        }
                      });
                    }
                  }}
                >
                  <Fontawesome name="trash" />
                </button>
              );
            }}
          </Mutation>
        </td>
      </tr>
    );
  }
}

export default injectSheet(styles)(UserRow);
