export default (theme) => ({
  ...theme.common,
  addUserForm: {
    display: 'flex',
    flexDirection: 'column',
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.s,

    '& label': {
      color: '#666',
      fontSize: theme.font.size.xs,
      margin: `0 0 0.3rem 0.2rem`
    },

    '& input': {
      border: '1px solid #ccc',
      borderRadius: '2px',
      margin: `0 0 ${theme.spacing.xs} 0`,
      padding: theme.spacing.xs
    }
  },
  error: {
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.xs,
    margin: `0 0 ${theme.spacing.s} 0`
  },
  success: {
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.xs,
    margin: `0 0 ${theme.spacing.s} 0`
  }
});
