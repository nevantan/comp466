export default (theme) => ({
  ...theme.common,
  content: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    minHeight: '100vh',
    padding: theme.spacing.xs
  },
  controls: {
    marginTop: 'auto'
  },
  '@media(max-width: 768px)': {
    content: {
      paddingTop: '7rem'
    }
  }
});
