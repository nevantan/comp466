// React
import React from 'react';

// React Router
import { Redirect } from 'react-router-dom';

// GraphQL
import { Mutation } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style.js';

// Components
import CourseCard from '../../CourseCard';

export const RESET_PASSWORD = gql`
  mutation ResetPassword($userid: ID!, $password: String!) {
    resetPassword(id: $userid, password: $password)
  }
`;

export class ResetPassword extends React.Component {
  constructor(props) {
    super(props);

    props.setTitle('Reset Password');
    props.setParent({});

    this.state = {
      password: '',
      confirm: '',
      error: '',
      valid: false,
      timeout: -1
    };
  }

  validate = () => {
    if (this.state.password.length > 0 && this.state.confirm.length > 0) {
      if (this.state.password !== this.state.confirm) {
        this.setState({
          error: 'Passwords must match.'
        });
      } else {
        this.setState({
          error: '',
          valid: true
        });
      }
    }
  };

  validateForm = (e) => {
    clearTimeout(this.state.timeout);
    this.setState({
      timeout: setTimeout(this.validate, 300)
    });
  };

  onPasswordInput = (e) => {
    this.setState(
      {
        password: e.target.value,
        error: ''
      },
      this.validateForm
    );
  };

  onConfirmInput = (e) => {
    this.setState(
      {
        confirm: e.target.value,
        error: ''
      },
      this.validateForm
    );
  };

  submitForm = (e) => {
    e.preventDefault();
  };

  render() {
    const { classes, userid, ...props } = this.props;
    return (
      <div className={classes.resetPage}>
        <div className={classes.formWrap}>
          <h2>Reset Password</h2>
          <form onSubmit={this.submitForm}>
            <div className={classes.inputWrap}>
              <label
                htmlFor="newpass"
                className={`${this.state.valid && 'valid'}`}
              >
                New Password
              </label>
              <input
                id="newpass"
                type="password"
                className={`${classes.input} ${this.state.valid && 'valid'}`}
                value={this.state.password}
                onChange={this.onPasswordInput}
              />
            </div>

            <div className={classes.inputWrap}>
              <label
                htmlFor="confirmpass"
                className={`${this.state.valid && 'valid'}`}
              >
                Repeat Password
              </label>
              <input
                id="confirmpass"
                type="password"
                className={`${classes.input} ${this.state.valid && 'valid'}`}
                value={this.state.confirm}
                onChange={this.onConfirmInput}
              />

              {this.state.error && (
                <div className={classes.error}>
                  <span> </span>
                  {this.state.error}
                </div>
              )}
            </div>

            <Mutation mutation={RESET_PASSWORD}>
              {(resetPassword, { loading, error, data }) => {
                if (loading)
                  return <div className={classes.loading}>Loading...</div>;
                if (error)
                  return <div className={classes.error}>{error.message}</div>;
                if (data && data.resetPassword) {
                  return <Redirect to="/" />;
                }

                return (
                  <button
                    className={classes.btnPrimary}
                    onClick={(e) => {
                      if (this.state.valid) {
                        resetPassword({
                          variables: {
                            userid,
                            password: this.state.password
                          }
                        });
                      }
                    }}
                  >
                    Reset Password
                  </button>
                );
              }}
            </Mutation>
          </form>
        </div>
      </div>
    );
  }
}

export default injectSheet(styles)(ResetPassword);
