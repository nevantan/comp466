export default (theme) => ({
  ...theme.common,
  resetPage: {
    display: 'flex',
    justifyContent: 'center'
  },
  inputWrap: {
    display: 'flex',
    flexDirection: 'column',
    position: 'relative'
  },
  error: {
    backgroundColor: '#880000',
    borderRadius: '2px',
    bottom: 0,
    color: '#fff',
    left: 'calc(100% + 1.9rem)',
    margin: `0 0 ${theme.spacing.xs} 0`,
    padding: theme.spacing.xs,
    position: 'absolute',
    textAlign: 'center',
    width: '20rem',

    '& span': {
      backgroundColor: '#800',
      height: '2.7rem',
      left: '-1.4rem',
      position: 'absolute',
      top: '0.55rem',
      transform: 'rotate(45deg)',
      width: '2.7rem'
    }
  },
  formWrap: {
    backgroundColor: theme.color.light,
    border: '1px solid #eee',
    borderRadius: '2px',
    boxShadow: '1px 1px 1px 1px rgba(0, 0, 0, 0.2)',
    fontSize: theme.font.size.xs,
    margin: `5% 0`,
    maxWidth: '25rem',
    padding: theme.spacing.s,
    width: '100%',

    '& h2': {
      fontSize: theme.font.size.m,
      fontWeight: 'normal',
      margin: `0 0 ${theme.spacing.s} 0`
    },

    '& form': {
      display: 'flex',
      flexDirection: 'column',
      fontFamily: theme.font.family.body,

      '& label': {
        color: '#666',
        margin: `0 0 0.3rem 0`
      },

      '& label.error': {
        color: '#800'
      },

      '& label.valid': {
        color: '#080'
      },

      '& input[type="password"]': {
        border: '1px solid #ccc',
        borderRadius: '2px',
        fontFamily: theme.font.family.body,
        padding: theme.spacing.xs,
        margin: `0 0 ${theme.spacing.xs} 0`
      },

      '& input.error': {
        border: '1px solid #800'
      },

      '& input.valid': {
        border: '1px solid #080'
      }
    }
  }
});
