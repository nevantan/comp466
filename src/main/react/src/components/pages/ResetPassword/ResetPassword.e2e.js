import puppeteer from 'puppeteer';

import { MongoClient, ObjectId } from 'mongodb';

const testAccounts = [
  {
    _id: new ObjectId('5bd371f624bb9a0001c6d85e'),
    email: 'john@example.com',
    password: 'asdfasdfasdf',
    enrolments: [],
    isTeacher: false,
    temp_pass: true
  }
];

beforeAll(async () => {
  const url = 'mongodb://localhost:27017';
  let client = null;

  try {
    client = await MongoClient.connect(
      url,
      {
        useNewUrlParser: true
      }
    );

    const db = await client.db('elearning');
    const users = await db.collection('users');
    await users.insertMany(testAccounts);
  } catch (e) {
    console.error(e);
  } finally {
    client.close();
  }
}, 10000);

afterAll(async () => {
  const url = 'mongodb://localhost:27017';
  let client = null;

  try {
    client = await MongoClient.connect(
      url,
      {
        useNewUrlParser: true
      }
    );

    const db = await client.db('elearning');
    const users = await db.collection('users');

    testAccounts.forEach((account) => {
      users.deleteOne({ _id: account._id });
    });
  } catch (e) {
    console.error(e);
  } finally {
    client.close();
  }
}, 10000);

describe(
  'the All Courses page',
  () => {
    let browser;
    let page;

    beforeEach(async () => {
      // Setup puppeteer
      browser = await puppeteer.launch({
        headless: true
      });
      page = await browser.newPage();

      page.emulate({
        viewport: {
          width: 800,
          height: 600
        },
        userAgent: ''
      });

      // Goto homepage and wait for title
      await page.goto('http://127.0.0.1:8080/');
      await page.waitForSelector('.card h2');

      // Make sure we are on the login page
      let title = await page.$eval('.card h2', (e) => e.innerText);
      expect(title).toBe('Login');

      // Given I am logged in
      await page.type('#email', testAccounts[0].email);
      await page.type('#password', testAccounts[0].password);
      await page.click('[type="submit"]');

      await page.waitForSelector('#app h1');

      title = await page.$eval('#app h1', (e) => e.innerText);
      expect(title).toBe('Reset Password');
    }, 10000);

    it(
      'passes',
      () => {
        expect(true).toBe(true);
      },
      10000
    );
  },
  10000
);
