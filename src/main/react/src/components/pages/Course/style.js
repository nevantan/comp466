export default (theme) => ({
  course: {
    display: 'flex',
    maxWidth: '100vw',
    overflowX: 'hidden'
  },
  unit: {
    alignItems: 'center',
    cursor: 'pointer',
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing.s,
    position: 'relative'
  },
  '@media(max-width: 768px)': {
    course: {
      padding: '6rem 0 0 0'
    },
    content: {
      transition: 'margin 200ms ease-in-out'
    },
    content_open: {
      margin: '0 0 0 25rem'
    },
    content_closed: {
      margin: 0
    }
  }
});
