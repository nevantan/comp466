Feature: Course details page

  Background:
    Given I am logged in
    And I am on the ECMA200 course page

  Scenario: I want to expand the second section
    Given the first section is expanded
    When I click the header of the second section
    Then the second section should expand
    And the first section should collapse

  Scenario: I want to view a lesson in the course
    Given the second section is expanded
    When I click the 'Lesson 1' link
    Then I should see the content for Lesson 1

  Scenario: I want to view a quiz I have not yet completed
    Given the second section is expanded
    And I have not yet completed 'Quiz 1'
    When I click the 'Quiz 1' link
    Then I should see the content for Quiz 1

  Scenario: I want to get a perfect grade on the quiz
    Given I am on the Quiz 1 page
    And I have selected both of the correct answers
    When I submit the quiz
    Then I should see a perfect grade (100%)

  Scenario: I want to get a passing grade on the quiz
    Given I am on the Quiz 1 page
    And I have selected some correct answers
    When I submit the quiz
    Then I should see a passing grade (50%)

  Scenario: I want to get a failing grade on the quiz
    Given I am on the Quiz 1 page
    And I have selected all incorrect answers
    When I submit the quiz
    Then I should see a failing grade (0%)

  Scenario: I want to view the results of a quiz I have already completed
    Given the second section is expanded
    And I have completed 'Quiz 1' with a perfect score
    When I click the 'Quiz 1' link
    Then I should see the results for Quiz 1