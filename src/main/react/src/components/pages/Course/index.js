// React
import React from 'react';
import ReactDOM from 'react-dom';

// React Router
import { Route, Redirect } from 'react-router-dom';

// GraphQL
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

// Components
import CourseContent from './components/CourseContent';
import QuizPage from './components/QuizPage';
import QuizResultsPage from './components/QuizResultsPage';
import Sidebar from '../../Sidebar';
import UnitMenuItem from '../../Sidebar/components/UnitMenuItem';

export const COURSE_DETAILS = gql`
  query CourseAndUser($id: ID!) {
    course(id: $id) {
      id
      title
      code
      description
      units {
        id
        title
        pages {
          id
          type
          title
        }
      }
    }
    user_id @client
  }
`;

export class Course extends React.Component {
  constructor(props) {
    super(props);

    props.setTitle('Loading...');
    props.setParent({ link: '/', label: 'All Courses' });

    this.state = {
      expanded: 0,
      user_id: null,
      sidebar_open: false
    };
  }

  setCurrentExpanded = (i) => (prevState) => ({
    expanded: prevState.expanded === i ? -1 : i
  });

  setExpanded = (i) => {
    return (e) => {
      this.setState(this.setCurrentExpanded(i));
    };
  };

  renderQuizPage = (data) => (props) => (
    <QuizPage userid={data.user_id} {...props} />
  );

  renderResultsPage = (data) => (props) => (
    <QuizResultsPage userid={data.user_id} {...props} />
  );

  toggleSidebarState = (prevState) => ({
    sidebar_open: !prevState.sidebar_open
  });

  toggleSidebar = (e) => {
    this.setState(this.toggleSidebarState);
  };

  closeSidebar = (e) => {
    this.setState({
      sidebar_open: false
    });
  };

  render() {
    const { classes, match, title, setTitle, ...props } = this.props;
    return (
      <Query
        query={COURSE_DETAILS}
        variables={{ id: match.params.course_id }}
        onCompleted={(data) => {
          if (title === 'Loading...') {
            setTitle(data.course.title);
          }
        }}
      >
        {({ loading, error, data }) => {
          if (loading) return <div className={classes.loading}>Loading...</div>;
          if (error)
            return <div className={classes.error}>{error.message}</div>;

          return (
            <div className={classes.course}>
              <Sidebar
                expanded={this.state.sidebar_open}
                toggle={this.toggleSidebar}
              >
                <ul>
                  {data.course.units.map((unit, i) => (
                    <UnitMenuItem
                      key={i}
                      classes={classes}
                      cid={data.course.id}
                      unit={unit}
                      expanded={this.state.expanded === i}
                      onClick={this.setExpanded(i)}
                      closeSidebar={this.closeSidebar}
                    />
                  ))}
                </ul>
              </Sidebar>
              <div
                className={`${classes.content} ${
                  this.state.sidebar_open
                    ? classes.content_open
                    : classes.content_closed
                }`}
                onClick={this.closeSidebar}
              >
                <Route
                  path="/course/:course_id/:uid?/l/:pid?"
                  component={CourseContent}
                />
                <Route
                  path="/course/:course_id/:uid?/q/:pid?"
                  render={this.renderQuizPage(data)}
                />
                <Route
                  path="/course/:course_id/:uid?/r/:pid?"
                  render={this.renderResultsPage(data)}
                />
                {/*<Redirect
                  to={`/course/${data.course.id}/${data.course.units[0].id}/l/${
                    data.course.units[0].pages[0].id
                  }`}
                />*/}
              </div>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default injectSheet(styles)(Course);
