import puppeteer from 'puppeteer';

describe('the Courses page', () => {
  let browser;
  let page;

  beforeEach(async () => {
    // Setup puppeteer
    browser = await puppeteer.launch({
      headless: true
    });
    page = await browser.newPage();

    page.emulate({
      viewport: {
        width: 800,
        height: 600
      },
      userAgent: ''
    });

    // Goto homepage and wait for title
    await page.goto('http://127.0.0.1:8080/');
    await page.waitForSelector('.card h2');

    // Make sure we are on the login page
    let title = await page.$eval('.card h2', (e) => e.innerText);
    expect(title).toBe('Login');

    // Given I am logged in
    await page.type('#email', process.env.EMAIL);
    await page.type('#password', process.env.PASSWORD);
    await page.click('[type="submit"]');

    await page.waitForSelector('#app h1');

    // Ensure I am on the All Courses page
    title = await page.$eval('#app h1', (e) => e.innerText);
    expect(title).toBe('All Courses');

    // Given I am on the ECMA200 course page
    const course_card = await page.$x(
      "//a[contains(text(), 'Introduction to JavaScript')]"
    );
    await course_card[0].click();

    await page.waitForSelector('#app h1');

    title = await page.$eval('#app h1', (e) => e.innerText);
    expect(title).toBe('Introduction to JavaScript');
  }, 10000);

  it(
    'allows me to expand the second section',
    async () => {
      // Given the first section is expanded
      const menuItem0 = await page.$x(
        "//a[contains(text(), 'Welcome to ECMA200')]"
      );

      // When I click the header of the second section
      await page.click('#unit1');

      // Then the section section should expand
      const menuItem1 = await page.$x(
        "//a[contains(text(), 'Lesson 1: Getting Started')]"
      );
      expect(menuItem1.length).toBe(1);

      // And the first section should collapse
      expect(
        (await page.$x("//a[contains(text(), 'Welcome to ECMA200')]")).length
      ).toBe(0);
    },
    10000
  );

  it(
    'allows me to view a lesson in the course',
    async () => {
      // Given the second section is expanded
      await page.click('#unit1');

      // When I click the 'Lesson 1' link
      const menuItem1 = await page.$x(
        "//a[contains(text(), 'Lesson 1: Getting Started')]"
      );
      await menuItem1[0].click();

      await page.waitForSelector('#app h2');

      // Then I should see the content for Lesson 1
      const title = await page.$eval('#app h2', (e) => e.innerText);
      expect(title).toBe('Lesson 1: Getting Started');
    },
    10000
  );

  it('allows me to view a quiz I have not yet completed', async () => {
    // Given the second section is expanded
    await page.click('#unit1');

    // And I have not yet completed Quiz 1

    // When I click the Quiz 1 link
    const menuItem1 = await page.$x(
      "//a[contains(text(), 'Quiz 1: Basic Syntax')]"
    );
    await menuItem1[0].click();

    await page.waitForSelector('#app h2');

    // Then I should see the content for Quiz 1
    const title = await page.$eval('#app h2', (e) => e.innerText);
    expect(title).toBe('Quiz 1: Basic Syntax');
  });

  afterEach(async () => {
    // Close browser
    await browser.close();
  }, 10000);
});
