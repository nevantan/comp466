import React from 'react';
import { shallow, mount } from 'enzyme';

// GraphQL
import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';
import { QUIZ_CONTENT, GRADE_QUIZ, QuizPage } from './';

// Styles
import { ThemeProvider } from 'react-jss';
import theme from '../../../../../styles/theme';

// Router
import { StaticRouter } from 'react-router-dom';

const courseResult = {
  course: {
    id: 'foo',
    unit: {
      id: 'bar',
      quiz: {
        id: 'bar',
        title: 'Test Quiz',
        questions: [
          {
            id: 'q1',
            prompt: 'This is the first test question.',
            ordered: true,
            responses: [
              { id: 'r0', text: 'This is the first response.' },
              { id: 'r1', text: 'This is the second response.' },
              { id: 'r2', text: 'This is the third response.' },
              { id: 'r3', text: 'This is the fourth response.' }
            ]
          },
          {
            id: 'q2',
            prompt: 'This is the second test question.',
            ordered: true,
            responses: [
              { id: 'r0', text: 'This is the first response.' },
              { id: 'r1', text: 'This is the second response.' },
              { id: 'r2', text: 'This is the third response.' },
              { id: 'r3', text: 'This is the fourth response.' }
            ]
          }
        ]
      }
    }
  }
};

const userResult = {
  user: {
    id: 'foobar',
    quiz: {
      id: ''
    }
  }
};

const queryResult = {
  data: {
    ...courseResult,
    ...userResult
  }
};

const mutationResult = {
  data: {
    gradeQuiz: {
      answers: [false, true]
    }
  }
};

const mocks = [
  {
    request: {
      query: QUIZ_CONTENT,
      variables: {
        userid: 'foobar',
        id: 'foo',
        uid: 'bar',
        pid: 'baz'
      }
    },
    result: { ...queryResult }
  },
  {
    request: {
      query: QUIZ_CONTENT,
      variables: {
        userid: 'foobar',
        id: 'error',
        uid: 'bar',
        pid: 'baz'
      }
    },
    error: new Error('This is a test error.')
  },
  {
    request: {
      query: GRADE_QUIZ,
      variables: {
        user: 'foobar',
        cid: 'foo',
        uid: 'bar',
        qid: 'baz',
        answers: ['r1', 'r2']
      }
    },
    result: { ...mutationResult }
  }
];

describe('by default', () => {
  it('renders the QuizPage component correctly', () => {
    const wrapper = shallow(<QuizPage classes={{}} match={{ params: {} }} />);
    expect(wrapper).toMatchSnapshot();
  });

  it('fetches data from a GraphQL endpoint correctly and shows the loading state', () => {
    const wrapper = mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(1);
    expect(wrapper.find('.loading').text()).toBe('Loading...');
  });

  it('fetches data from a GraphQL endpoint correctly and shows the final state', async () => {
    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    // Update past first query
    await wait(0);
    await wrapper.update();

    // Update past second query
    await wait(0);
    await wrapper.update();

    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(0);
    expect(wrapper).toMatchSnapshot();
  });

  it('fetches data from a GraphQL endpoint correctly and shows the error state', async () => {
    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'error',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(1);
    expect(wrapper.find('.error').text()).toBe(
      'Network error: This is a test error.'
    );
  });
});

describe('quiz submission', () => {
  it('should warn if there are unanswered questions', async () => {
    const confirm = jest.fn();
    confirm.mockReturnValue(false);

    global.confirm = confirm;

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    wrapper.find('#submit-quiz').simulate('click');
    expect(confirm).toHaveBeenCalledTimes(1);
    expect(confirm).toHaveBeenCalledWith(
      'Some questions have been left unanswered. Are you sure you want to submit?'
    );
  });

  it('should warn before submitting', async () => {
    const confirm = jest.fn();
    confirm.mockReturnValueOnce(true);
    confirm.mockReturnValue(false);

    global.confirm = confirm;

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);

    wrapper.update();

    wrapper.find('#submit-quiz').simulate('click');
    expect(confirm).toHaveBeenCalledTimes(2);
    expect(confirm).toHaveBeenCalledWith(
      'Some questions have been left unanswered. Are you sure you want to submit?'
    );
    expect(confirm).toHaveBeenCalledWith(
      'Once you submit this quiz, you will be unable to change your answers. Are you sure you want to submit?'
    );
  });

  it('should successfully set submitted state', async () => {
    const confirm = jest.fn();
    confirm.mockReturnValue(true);

    global.confirm = confirm;

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizPage
              classes={{
                loading: 'loading',
                error: 'error',
                mutation_loading: 'mut_loading'
              }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    wrapper
      .find(QuizPage)
      .instance()
      .setState({
        choices: {
          q1: 'r1',
          q2: 'r2',
          _count: 2
        }
      });

    wrapper.find('#submit-quiz').simulate('click');
    wrapper.update();

    expect(confirm).toHaveBeenCalledTimes(1);
    expect(confirm).toHaveBeenCalledWith(
      'Once you submit this quiz, you will be unable to change your answers. Are you sure you want to submit?'
    );

    expect(wrapper.find('.mut_loading').text()).toBe('Submitting Answers...');
  });

  it('should load the mutation once submitted', async () => {
    const confirm = jest.fn();
    confirm.mockReturnValue(true);

    global.confirm = confirm;

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizPage
              classes={{
                loading: 'loading',
                error: 'error',
                submitted: 'submitted',
                mutation_loading: 'mut_loading',
                mutation_error: 'mut_error'
              }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    wrapper
      .find(QuizPage)
      .instance()
      .setState({
        choices: {
          q1: 'r1',
          q2: 'r2',
          _count: 2
        }
      });

    // Wait for quiz data to load from Query
    await wait(0);
    wrapper.update();

    // Wait for submit button to load from Mutation
    await wait(0);
    wrapper.update();

    wrapper.find('#submit-quiz').simulate('click');

    // Mutation should be in loading state, no error
    expect(wrapper.find('.mut_error').length).toBe(0);
    expect(wrapper.find('.mut_loading').length).toBe(1);

    expect(confirm).toHaveBeenCalledTimes(1);
    expect(confirm).toHaveBeenCalledWith(
      'Once you submit this quiz, you will be unable to change your answers. Are you sure you want to submit?'
    );

    // Wait for Mutation to finish loading the submit
    await wait(2);
    wrapper.update();

    expect(wrapper.find('.submitted').length).toBe(1);
  });
});
