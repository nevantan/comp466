export default (theme) => ({
  content: {
    maxWidth: '90rem',
    padding: `${theme.spacing.m} ${theme.spacing.s} 7rem ${theme.spacing.s}`,
    width: '100vw'
  },
  title: {
    fontFamily: theme.font.family.header,
    fontSize: theme.font.size.m,
    fontWeight: 'normal',
    marginTop: 0
  },
  result: {
    borderRadius: '0.2rem',
    color: '#fff',
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.s,
    margin: `0 0 ${theme.spacing.xs} 0`,
    padding: theme.spacing.s,

    '&.pass': {
      backgroundColor: theme.color.pass
    },
    '&.fail': {
      backgroundColor: theme.color.fail
    }
  },
  question: {
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.s,

    '& code': {
      backgroundColor: '#ddd',
      padding: '0.3rem 0.5rem 0 0.5rem'
    }
  },
  prompt: {
    fontWeight: 'bold'
  },
  responses: {
    listStyle: 'none',
    paddingLeft: theme.spacing.xs
  },
  response: {
    alignItems: 'center',
    display: 'flex',
    height: '2.4rem',
    margin: `0 0 ${theme.spacing.xs} 0`,

    '& div, & p, & pre': {
      display: 'inline',
      margin: 0,
      padding: 0
    },

    '& .fa': {
      margin: `0 ${theme.spacing.s} 0 0`,
      textAlign: 'center',
      width: '1.8rem'
    },

    '& .fa-circle-notch': {
      color: 'transparent'
    }
  },
  check: {
    color: theme.color.pass
  },
  times: {
    color: theme.color.fail
  }
});
