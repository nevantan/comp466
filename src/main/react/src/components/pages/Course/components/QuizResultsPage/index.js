// React
import React from 'react';
import FontAwesome from 'react-fontawesome';

// Routing
import { Redirect } from 'react-router-dom';

// GraphQL
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

// Components
import Question from './components/Question';

export const FETCH_RESULTS = gql`
  query GetQuizResults($user: ID!, $cid: ID!, $uid: ID!, $pid: ID!) {
    user(id: $user) {
      id
      quiz(cid: $cid, uid: $uid, pid: $pid) {
        id
        responses
        correct
        grade
        pass
      }
    }
    course(id: $cid) {
      id
      unit(id: $uid) {
        id
        quiz(id: $pid) {
          id
          title
          total
          questions {
            id
            prompt
            answer
            ordered
            responses {
              id
              text
            }
          }
        }
      }
    }
  }
`;

export class QuizResultsPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      cid: props.match.params.course_id,
      uid: props.match.params.uid,
      pid: props.match.params.pid
    };
  }

  render() {
    const { classes, match, userid, ...props } = this.props;

    return (
      <Query
        query={FETCH_RESULTS}
        variables={{
          user: userid,
          cid: match.params.course_id,
          uid: match.params.uid,
          pid: match.params.pid
        }}
      >
        {({ loading, error, data }) => {
          if (loading)
            return <div className={classes.loading}>Loading results...</div>;
          if (error)
            return <div className={classes.error}>{error.message}</div>;

          const quiz = data.course.unit.quiz;
          const user = data.user.quiz;
          const result = user.pass ? 'pass' : 'fail';
          return (
            <div className={classes.content}>
              <h1 className={classes.title}>{quiz.title} (Results)</h1>
              <div className={`${classes.result} ${result}`}>
                {user.pass ? 'Pass: ' : 'Fail: '}
                {user.correct}/{quiz.total} ({user.grade * 100}%)
              </div>
              {quiz.questions.map((question, i) => (
                <Question
                  classes={classes}
                  question={question}
                  choice={user.responses[i]}
                  key={i}
                />
              ))}
            </div>
          );
        }}
      </Query>
    );
  }
}

export default injectSheet(styles)(QuizResultsPage);
