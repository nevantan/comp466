import React from 'react';
import { shallow, mount } from 'enzyme';

// GraphQL
import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';
import { LESSON_CONTENT, CourseContent } from './';

// Styles
import { ThemeProvider } from 'react-jss';
import theme from '../../../../../styles/theme';

// Router
import { StaticRouter } from 'react-router-dom';

const result = {
  data: {
    course: {
      id: 'foo',
      unit: {
        id: 'bar',
        lesson: {
          id: 'baz',
          title: 'Test Lesson',
          sections: [
            {
              title: 'Test Section 1',
              content: 'Some test content'
            },
            {
              title: 'Test Section 2',
              content: 'Some more test content'
            }
          ]
        }
      }
    }
  }
};
const mocks = [
  {
    request: {
      query: LESSON_CONTENT,
      variables: {
        id: 'foo',
        uid: 'bar',
        pid: 'baz'
      }
    },
    result: { ...result }
  },
  {
    request: {
      query: LESSON_CONTENT,
      variables: {
        id: 'error',
        uid: 'bar',
        pid: 'baz'
      }
    },
    error: new Error('This is a test error.')
  }
];

describe('by default', () => {
  it('renders the CourseContent component correctly', () => {
    const wrapper = shallow(
      <CourseContent classes={{}} match={{ params: {} }} />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('fetches data from a GraphQL endpoint correctly and shows the loading state', () => {
    const wrapper = mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <CourseContent
              classes={{ loading: 'loading', error: 'error' }}
              match={{ params: {} }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(1);
    expect(wrapper.find('.loading').text()).toBe('Loading...');
  });

  it('fetches data from a GraphQL endpoint correctly and shows the final state', async () => {
    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <CourseContent
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);

    wrapper.update();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(0);
  });

  it('fetches data from a GraphQL endpoint correctly and shows the error state', async () => {
    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <CourseContent
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'error',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);

    wrapper.update();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(1);
    expect(wrapper.find('.error').text()).toBe(
      'Network error: This is a test error.'
    );
  });
});
