import React from 'react';
import { shallow, mount } from 'enzyme';

// GraphQL
import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';
import { FETCH_RESULTS, QuizResultsPage } from './';

// Styles
import { ThemeProvider } from 'react-jss';
import theme from '../../../../../styles/theme';

// Router
import { StaticRouter } from 'react-router-dom';

const queryResult = {
  data: {
    user: {
      id: 'foobar',
      quiz: {
        id: 'foo',
        responses: ['r1', 'r2'],
        correct: 1,
        grade: 0.5,
        pass: true
      }
    },
    course: {
      id: 'foo',
      unit: {
        id: 'unit1',
        quiz: {
          id: 'quiz1',
          title: 'Test Quiz',
          total: 2,
          questions: [
            {
              id: 'q1',
              prompt: 'This is the first test question.',
              ordered: true,
              answer: 'r0',
              responses: [
                { id: 'r0', text: 'This is the first response.' },
                { id: 'r1', text: 'This is the second response.' },
                { id: 'r2', text: 'This is the third response.' },
                { id: 'r3', text: 'This is the fourth response.' }
              ]
            },
            {
              id: 'q2',
              prompt: 'This is the second test question.',
              ordered: true,
              answer: 'r2',
              responses: [
                { id: 'r0', text: 'This is the first response.' },
                { id: 'r1', text: 'This is the second response.' },
                { id: 'r2', text: 'This is the third response.' },
                { id: 'r3', text: 'This is the fourth response.' }
              ]
            }
          ]
        }
      }
    }
  }
};

const mocks = [
  {
    request: {
      query: FETCH_RESULTS,
      variables: {
        user: 'foobar',
        cid: 'foo',
        uid: 'bar',
        pid: 'baz'
      }
    },
    result: { ...queryResult }
  },
  {
    request: {
      query: FETCH_RESULTS,
      variables: {
        user: 'foobarbaz',
        cid: 'foo',
        uid: 'bar',
        pid: 'baz'
      }
    },
    result: {
      data: {
        ...queryResult.data,
        user: {
          id: 'foobar',
          quiz: {
            id: 'foo',
            responses: ['r3', 'r3'],
            correct: 0,
            grade: 0,
            pass: false
          }
        }
      }
    }
  },
  {
    request: {
      query: FETCH_RESULTS,
      variables: {
        user: 'foobar',
        cid: 'error',
        uid: 'bar',
        pid: 'baz'
      }
    },
    error: new Error('This is a test error.')
  }
];

describe('by default', () => {
  it('renders the QuizResultsPage component correctly', () => {
    const wrapper = shallow(
      <QuizResultsPage classes={{}} match={{ params: {} }} />
    );
    expect(wrapper).toMatchSnapshot();
  });

  it('fetches data from a GraphQL endpoint correctly and shows the loading state', () => {
    const wrapper = mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizResultsPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(1);
    expect(wrapper.find('.loading').text()).toBe('Loading results...');
  });

  it('fetches data from a GraphQL endpoint correctly and shows the final state of a passed quiz', async () => {
    const wrapper = mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizResultsPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(0);
  });

  it('fetches data from a GraphQL endpoint correctly and shows the final state of a failed quiz', async () => {
    const wrapper = mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizResultsPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'foo',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobarbaz"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(0);
  });

  it('fetches data from a GraphQL endpoint correctly and shows the error state', async () => {
    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <QuizResultsPage
              classes={{ loading: 'loading', error: 'error' }}
              match={{
                params: {
                  course_id: 'error',
                  uid: 'bar',
                  pid: 'baz'
                }
              }}
              userid="foobar"
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(1);
    expect(wrapper.find('.error').text()).toBe(
      'Network error: This is a test error.'
    );
  });
});
