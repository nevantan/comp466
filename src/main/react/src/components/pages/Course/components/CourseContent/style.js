import color from 'color';

export default (theme) => ({
  content: {
    maxWidth: '90rem',
    padding: `${theme.spacing.m} ${theme.spacing.s} 7rem ${theme.spacing.s}`,
    width: '100vw',

    '& img': {
      display: 'block',
      height: 'auto',
      maxWidth: '100%',
      margin: '0 0 2rem 0'
    }
  },
  title: {
    fontFamily: theme.font.family.header,
    fontSize: theme.font.size.m,
    fontWeight: 'normal',
    marginTop: 0
  },
  section: {
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.s,
    maxWidth: '100%',

    '& code': {
      backgroundColor: '#ddd'
    }
  }
});
