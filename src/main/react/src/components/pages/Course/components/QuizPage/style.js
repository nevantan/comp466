import color from 'color';

export default (theme) => ({
  ...theme.common,
  btnWrap: {
    display: 'flex',
    justifyContent: 'flex-end',
    padding: `${theme.spacing.s} 0 0 0`,
    width: '100%'
  },
  content: {
    maxWidth: '90rem',
    padding: theme.spacing.m,
    width: '100vw'
  },
  title: {
    fontFamily: theme.font.family.header,
    fontSize: theme.font.size.m,
    fontWeight: 'normal',
    marginTop: 0
  },
  section: {
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.s,

    '& code': {
      backgroundColor: '#ddd'
    }
  },
  question: {
    fontFamily: theme.font.family.body,
    fontSize: theme.font.size.s,

    '& code': {
      backgroundColor: '#ddd',
      padding: '0.3rem 0.5rem'
    }
  },
  prompt: {
    fontWeight: 'bold'
  },
  responses: {
    listStyle: 'none'
  },
  response: {
    alignItems: 'center',
    display: 'flex',
    margin: `0 0 ${theme.spacing.xs} 0`,

    '& label': {
      marginLeft: theme.spacing.xs,

      '& div, & p, & pre': {
        display: 'inline',
        margin: 0
      }
    }
  },
  submitted: {
    alignItems: 'center',
    color: theme.color.primary,
    display: 'flex',
    fontSize: theme.font.size.m
  },
  redirect_text: {
    color: '#999',
    fontSize: theme.font.size.xs,
    marginRight: theme.spacing.xs
  }
});
