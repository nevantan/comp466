// React
import React from 'react';
import Markdown from 'react-markdown';
import PropTypes from 'prop-types';

const Question = ({ classes, question, choice, setChoice, ...props }) => (
  <div className={classes.question}>
    <div className={classes.prompt}>{question.prompt}</div>
    <ul className={classes.responses}>
      {question.responses.map((response, i) => (
        <li className={classes.response} key={i}>
          <input
            id={`${question.id}-${response.id}`}
            checked={response.id === choice}
            onChange={() => {
              setChoice(question.id, response.id);
            }}
            type="radio"
            name={question.id}
          />
          <label htmlFor={`${question.id}-${response.id}`}>
            <Markdown source={response.text} />
          </label>
        </li>
      ))}
    </ul>
  </div>
);

Question.propTypes = {
  classes: PropTypes.object.isRequired,
  question: PropTypes.shape({
    id: PropTypes.string.isRequired,
    prompt: PropTypes.string.isRequired,
    responses: PropTypes.array.isRequired
  }),
  choice: PropTypes.string,
  setChoice: PropTypes.func.isRequired
};

export default Question;
