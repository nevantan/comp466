import React from 'react';
import { shallow } from 'enzyme';

import Question from './';

describe('by default', () => {
  it('correctly renders the Question component with no responses', () => {
    const question = {
      id: 'q1',
      answer: '',
      prompt: 'This is a test prompt.',
      responses: []
    };

    const wrapper = shallow(
      <Question classes={{}} question={question} choice="" />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('correctly renders the Question component with 4 responses and a correct answer', () => {
    const question = {
      id: 'q1',
      prompt: 'This is a test prompt.',
      answer: 'r1',
      responses: [
        { id: 'r1', text: 'First response.' },
        { id: 'r2', text: 'Second response.' },
        { id: 'r3', text: 'Third response.' },
        { id: 'r4', text: 'Fourth response.' }
      ]
    };

    const wrapper = shallow(
      <Question classes={{}} question={question} choice="r1" />
    );

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('[name="check"]').length).toBe(1);
    expect(wrapper.find('[name="circle-notch"]').length).toBe(3);
    expect(wrapper.find('[name="times"]').length).toBe(0);
  });

  it('correctly renders the Question component with 4 responses and an incorrect answer', () => {
    const question = {
      id: 'q1',
      prompt: 'This is a test prompt.',
      answer: 'r2',
      responses: [
        { id: 'r1', text: 'First response.' },
        { id: 'r2', text: 'Second response.' },
        { id: 'r3', text: 'Third response.' },
        { id: 'r4', text: 'Fourth response.' }
      ]
    };

    const wrapper = shallow(
      <Question classes={{}} question={question} choice="r1" />
    );

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('[name="check"]').length).toBe(1);
    expect(wrapper.find('[name="circle-notch"]').length).toBe(2);
    expect(wrapper.find('[name="times"]').length).toBe(1);
  });
});
