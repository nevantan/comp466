// React
import React from 'react';
import FontAwesome from 'react-fontawesome';

// Routing
import { Redirect } from 'react-router-dom';

// Markdown
import Markdown from 'react-markdown';

// GraphQL
import { Query, Mutation } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

// Components
import Question from './components/Question';

export const QUIZ_CONTENT = gql`
  query course($userid: ID!, $id: ID!, $uid: ID!, $pid: ID!) {
    course(id: $id) {
      id
      unit(id: $uid) {
        id
        quiz(id: $pid) {
          id
          title
          questions {
            id
            prompt
            ordered
            responses {
              id
              text
            }
          }
        }
      }
    }

    user(id: $userid) {
      id
      quiz(cid: $id, uid: $uid, pid: $pid) {
        id
      }
    }
  }
`;

export const GRADE_QUIZ = gql`
  mutation DoGradeQuiz(
    $user: ID!
    $cid: ID!
    $uid: ID!
    $qid: ID!
    $answers: [ID]!
  ) {
    gradeQuiz(user: $user, cid: $cid, uid: $uid, qid: $qid, answers: $answers)
  }
`;

export class QuizPage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      choices: {
        _count: -1
      },
      submitted: false,
      cid: props.match.params.course_id,
      uid: props.match.params.uid,
      pid: props.match.params.pid,
      redirect: false
    };
  }

  setChoice = (id, choice) => {
    this.setState((prevState) => ({
      choices: {
        ...prevState.choices,
        [id]: choice
      }
    }));
  };

  render() {
    const { classes, match, userid, ...props } = this.props;

    return (
      <Query
        query={QUIZ_CONTENT}
        variables={{
          userid: userid,
          id: this.state.cid,
          uid: this.state.uid || 'unit0',
          pid: this.state.pid || 'quiz0'
        }}
        onCompleted={(data) => {
          if (this.state.choices._count === -1) {
            const choices = {};
            data.course.unit.quiz.questions.forEach((question) => {
              choices[question.id] = '';
            });
            choices._count = data.course.unit.quiz.questions.length;
            this.setState({
              choices
            });
          }
        }}
      >
        {({ loading, error, data }) => {
          if (loading) return <div className={classes.loading}>Loading...</div>;
          if (error) {
            return <div className={classes.error}>{error.message}</div>;
          }

          if (data.user.quiz.id !== '')
            return (
              <Redirect
                to={`/course/${this.state.cid}/${this.state.uid}/r/${
                  this.state.pid
                }`}
              />
            );

          const page = data.course.unit.quiz;
          return (
            <div className={classes.content}>
              <h2 className={classes.title}>{page.title}</h2>
              {page.questions.map((question, i) => (
                <Question
                  classes={classes}
                  question={question}
                  choice={this.state.choices[question.id]}
                  setChoice={this.setChoice}
                  key={i}
                />
              ))}
              <div className={classes.btnWrap}>
                <Mutation mutation={GRADE_QUIZ}>
                  {(gradeQuiz, { loading, error, data }) => {
                    if (loading)
                      return (
                        <button
                          className={`${classes.btnDisabled} ${
                            classes.mutation_loading
                          }`}
                          disabled
                        >
                          Submitting Answers...
                        </button>
                      );
                    if (error)
                      return (
                        <div className={classes.mutation_error}>
                          {error.message}
                        </div>
                      );

                    if (data) {
                      setTimeout(() => {
                        this.setState({
                          redirect: true
                        });
                      }, 2000);
                      return (
                        <div className={classes.submitted}>
                          <span className={classes.redirect_text}>
                            Redirecting...
                          </span>
                          <FontAwesome name="check-circle" />
                          {this.state.redirect && (
                            <Redirect
                              to={`/course/${this.state.cid}/${
                                this.state.uid
                              }/r/${this.state.pid}`}
                            />
                          )}
                        </div>
                      );
                    }

                    return (
                      <button
                        id="submit-quiz"
                        className={classes.btnPrimary}
                        onClick={(e) => {
                          const missedMessage =
                            'Some questions have been left unanswered. Are you sure you want to submit?';

                          const confirmMessage =
                            'Once you submit this quiz, you will be unable to change your answers. Are you sure you want to submit?';

                          if (
                            Object.values(this.state.choices).some(
                              (choice) => choice === ''
                            ) &&
                            !window.confirm(missedMessage)
                          )
                            return;

                          if (!window.confirm(confirmMessage)) return;

                          e.preventDefault();

                          const { _count, ...answers } = this.state.choices;
                          gradeQuiz({
                            variables: {
                              user: this.props.userid,
                              cid: this.state.cid,
                              uid: this.state.uid,
                              qid: this.state.pid,
                              answers: Object.values(answers)
                            }
                          });
                        }}
                      >
                        Submit Attempt
                      </button>
                    );
                  }}
                </Mutation>
              </div>
            </div>
          );
        }}
      </Query>
    );
  }
}

export default injectSheet(styles)(QuizPage);
