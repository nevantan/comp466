// React
import React from 'react';
import Fontawesome from 'react-fontawesome';
import Markdown from 'react-markdown';
import PropTypes from 'prop-types';

export const icon = (answer, response, choice) => {
  if (answer == response) return 'check';
  if (response == choice) return 'times';
  return 'circle-notch';
};

const Question = ({ classes, question, choice, ...props }) => (
  <div className={classes.question}>
    <div className={classes.prompt}>{question.prompt}</div>
    <ul className={classes.responses}>
      {question.responses.map((response, i) => (
        <li
          className={`${classes.response} ${
            classes[icon(question.answer, response.id, choice)]
          }`}
          key={i}
        >
          <Fontawesome name={icon(question.answer, response.id, choice)} />
          <Markdown source={response.text} />
        </li>
      ))}
    </ul>
  </div>
);

Question.propTypes = {
  classes: PropTypes.object.isRequired,
  question: PropTypes.shape({
    id: PropTypes.string.isRequired,
    prompt: PropTypes.string.isRequired,
    answer: PropTypes.string.isRequired,
    responses: PropTypes.array.isRequired
  }),
  choice: PropTypes.string.isRequired
};

export default Question;
