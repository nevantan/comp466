import React from 'react';
import { shallow } from 'enzyme';

import Question from './';

describe('by default', () => {
  it('correctly renders the Question component with no responses', () => {
    const question = {
      id: 'q1',
      prompt: 'This is a test prompt.',
      responses: []
    };

    const wrapper = shallow(
      <Question
        classes={{}}
        question={question}
        choice={''}
        setChoice={() => {}}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('correctly renders the Question component with 4 responses', () => {
    const question = {
      id: 'q1',
      prompt: 'This is a test prompt.',
      responses: [
        { id: 'r1', text: 'First response.' },
        { id: 'r2', text: 'Second response.' },
        { id: 'r3', text: 'Third response.' },
        { id: 'r4', text: 'Fourth response.' }
      ]
    };

    const wrapper = shallow(
      <Question
        classes={{}}
        question={question}
        choice={''}
        setChoice={() => {}}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('correctly renders the Question component with 4 responses and a selection', () => {
    const question = {
      id: 'q1',
      prompt: 'This is a test prompt.',
      responses: [
        { id: 'r1', text: 'First response.' },
        { id: 'r2', text: 'Second response.' },
        { id: 'r3', text: 'Third response.' },
        { id: 'r4', text: 'Fourth response.' }
      ]
    };

    const wrapper = shallow(
      <Question
        classes={{}}
        question={question}
        choice={'r2'}
        setChoice={() => {}}
      />
    );

    expect(wrapper).toMatchSnapshot();
  });

  it('correctly selects a response', () => {
    const question = {
      id: 'q1',
      prompt: 'This is a test prompt.',
      responses: [
        { id: 'r0', text: 'First response.' },
        { id: 'r1', text: 'Second response.' },
        { id: 'r2', text: 'Third response.' },
        { id: 'r3', text: 'Fourth response.' }
      ]
    };

    const setChoice = jest.fn();
    const wrapper = shallow(
      <Question
        classes={{}}
        question={question}
        choice={''}
        setChoice={setChoice}
      />
    );

    wrapper.find('#q1-r0').simulate('change');
    expect(setChoice).toHaveBeenCalledTimes(1);
    expect(setChoice).toHaveBeenCalledWith('q1', 'r0');

    wrapper.find('#q1-r1').simulate('change');
    expect(setChoice).toHaveBeenCalledTimes(2);
    expect(setChoice).toHaveBeenCalledWith('q1', 'r1');

    wrapper.find('#q1-r2').simulate('change');
    expect(setChoice).toHaveBeenCalledTimes(3);
    expect(setChoice).toHaveBeenCalledWith('q1', 'r2');

    wrapper.find('#q1-r3').simulate('change');
    expect(setChoice).toHaveBeenCalledTimes(4);
    expect(setChoice).toHaveBeenCalledWith('q1', 'r3');
  });
});
