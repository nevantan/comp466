// React
import React from 'react';

// Markdown
import Markdown from 'react-markdown';

// GraphQL
import { Query } from 'react-apollo';
import gql from 'graphql-tag';

// Styles
import injectSheet from 'react-jss';
import styles from './style';

export const LESSON_CONTENT = gql`
  query course($id: ID!, $uid: ID!, $pid: ID!) {
    course(id: $id) {
      id
      unit(id: $uid) {
        id
        lesson(id: $pid) {
          id
          title
          sections {
            title
            content
          }
        }
      }
    }
  }
`;

export class CourseContent extends React.Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { classes, match, ...props } = this.props;

    return (
      <Query
        query={LESSON_CONTENT}
        variables={{
          id: match.params.course_id,
          uid: match.params.uid || 'unit0',
          pid: match.params.pid || 'lesson0'
        }}
      >
        {({ loading, error, data }) => {
          if (loading) return <div className={classes.loading}>Loading...</div>;
          if (error)
            return <div className={classes.error}>{error.message}</div>;

          const page = data.course.unit.lesson;
          return (
            <div className={classes.content}>
              <h2 className={classes.title}>{page.title}</h2>
              {page.sections.map((section, i) => (
                <div key={i} className={classes.section}>
                  <h3 className={classes.section_title}>{section.title}</h3>
                  <Markdown
                    source={section.content.replace(/((?!\n)\s{3,})/g, '')}
                  />
                </div>
              ))}
            </div>
          );
        }}
      </Query>
    );
  }
}

export default injectSheet(styles)(CourseContent);
