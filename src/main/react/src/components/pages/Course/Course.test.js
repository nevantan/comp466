import React from 'react';
import { shallow, mount } from 'enzyme';

// GraphQL
import { MockedProvider } from 'react-apollo/test-utils';
import wait from 'waait';
import { COURSE_DETAILS, Course } from './';

// Styles
import { ThemeProvider } from 'react-jss';
import theme from '../../../styles/theme';

// Router
import { StaticRouter } from 'react-router-dom';

const result = {
  data: {
    course: {
      id: 'foo',
      title: 'Test Course',
      code: 'FOO101',
      description: 'This is a test description.',
      units: [
        {
          id: 'unit1',
          title: 'Test Unit',
          pages: [
            {
              id: 'lesson1',
              title: 'Test Lesson 1',
              type: 'lesson'
            },
            {
              id: 'quiz1',
              title: 'Test Quiz 1',
              type: 'quiz'
            }
          ]
        }
      ]
    },
    user_id: 'foobar'
  }
};
const mocks = [
  {
    request: {
      query: COURSE_DETAILS,
      variables: { id: 'foo' }
    },
    result: { ...result }
  },
  {
    request: {
      query: COURSE_DETAILS,
      variables: { id: 'bar' }
    },
    result: { ...result }
  },
  {
    request: {
      query: COURSE_DETAILS,
      variables: { id: 'baz' }
    },
    error: new Error('This is a test error.')
  }
];

describe('by default', () => {
  it('renders the Course component correctly', () => {
    const setTitle = jest.fn();
    const setParent = jest.fn();

    const wrapper = shallow(
      <Course
        setTitle={setTitle}
        setParent={setParent}
        classes={{}}
        match={{ params: {} }}
      />
    );
    expect(wrapper).toMatchSnapshot();

    expect(setTitle).toHaveBeenCalledTimes(1);
    expect(setTitle).toHaveBeenCalledWith('Loading...');

    expect(setParent).toHaveBeenCalledTimes(1);
    expect(setParent).toHaveBeenCalledWith({ label: 'All Courses', link: '/' });
  });

  it('fetches data from GraphQL and renders the loading state', () => {
    const wrapper = mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={[...mocks]} addTypename={false}>
            <Course
              setTitle={() => {}}
              setParent={() => {}}
              classes={{ loading: 'loading', error: 'error', course: 'course' }}
              match={{ params: { course_id: 'foo' } }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.error').length).toBe(0);
    expect(wrapper.find('.loading').length).toBe(1);
    expect(wrapper.find('.loading').text()).toBe('Loading...');
  });

  it('fetches data from GraphQL and renders the final state', async () => {
    const setTitle = jest.fn();

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={[...mocks]} addTypename={false}>
            <Course
              title="Loading..."
              setTitle={setTitle}
              setParent={() => {}}
              classes={{ loading: 'loading', error: 'error' }}
              match={{ params: { course_id: 'bar' } }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    // Update past first Query
    await wait(0);
    wrapper.update();

    // Update past second Query
    await wait(0);
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(0);
    expect(setTitle).toHaveBeenCalledTimes(2);
    expect(setTitle).toHaveBeenCalledWith('Test Course');
  });

  it('fetches data from GraphQL and renders the error state', async () => {
    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter context={{}}>
          <MockedProvider mocks={mocks} addTypename={false}>
            <Course
              setTitle={() => {}}
              setParent={() => {}}
              classes={{ loading: 'loading', error: 'error' }}
              match={{ params: { course_id: 'baz' } }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);

    wrapper.update();
    expect(wrapper).toMatchSnapshot();
    expect(wrapper.find('.loading').length).toBe(0);
    expect(wrapper.find('.error').length).toBe(1);
    expect(wrapper.find('.error').text()).toBe(
      'Network error: This is a test error.'
    );
  });
});

describe('alternate routes', () => {
  it('renders the Lesson route', async () => {
    const setTitle = jest.fn();

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter
          location="http://127.0.0.1/course/bar/unit1/l/lesson1"
          context={{}}
        >
          <MockedProvider mocks={[...mocks]} addTypename={false}>
            <Course
              title="Loading..."
              setTitle={setTitle}
              setParent={() => {}}
              classes={{ loading: 'loading', error: 'error' }}
              match={{ params: { course_id: 'bar', uid: 'unit1' } }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
  });

  it('renders the Quiz route', async () => {
    const setTitle = jest.fn();

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter
          location="http://127.0.0.1/course/bar/unit1/q/quiz1"
          context={{}}
        >
          <MockedProvider mocks={[...mocks]} addTypename={false}>
            <Course
              title="Loading..."
              setTitle={setTitle}
              setParent={() => {}}
              classes={{ loading: 'loading', error: 'error' }}
              match={{ params: { course_id: 'bar', uid: 'unit1' } }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
  });

  it('renders the Quiz results route', async () => {
    const setTitle = jest.fn();

    const wrapper = await mount(
      <ThemeProvider theme={theme}>
        <StaticRouter
          location="http://127.0.0.1/course/bar/unit1/r/quiz1"
          context={{}}
        >
          <MockedProvider mocks={[...mocks]} addTypename={false}>
            <Course
              title="Loading..."
              setTitle={setTitle}
              setParent={() => {}}
              classes={{ loading: 'loading', error: 'error' }}
              match={{ params: { course_id: 'bar', uid: 'unit1' } }}
            />
          </MockedProvider>
        </StaticRouter>
      </ThemeProvider>
    );

    await wait(0);
    wrapper.update();

    expect(wrapper).toMatchSnapshot();
  });
});

describe('setExpanded callback', () => {
  it('sets the correct state', () => {
    const wrapper = shallow(
      <Course
        setTitle={() => {}}
        setParent={() => {}}
        classes={{}}
        match={{ params: {} }}
      />
    );
    expect(wrapper.state('expanded')).toBe(0);
    wrapper.instance().setExpanded(1)();
    expect(wrapper.state('expanded')).toBe(1);
  });
});

describe('route renders', () => {
  it('returns the correct QuizPage component', () => {
    const wrapper = shallow(
      <Course
        setTitle={() => {}}
        setParent={() => {}}
        classes={{}}
        match={{ params: {} }}
      />
    );

    expect(
      wrapper.instance().renderQuizPage({ userid: 'foobar' })({})
    ).toMatchSnapshot();
  });

  it('returns the correct QuizResultsPage component', () => {
    const wrapper = shallow(
      <Course
        setTitle={() => {}}
        setParent={() => {}}
        classes={{}}
        match={{ params: {} }}
      />
    );

    expect(
      wrapper.instance().renderResultsPage({ userid: 'foobar' })({})
    ).toMatchSnapshot();
  });
});

describe('setCurrentExpanded', () => {
  it('returns the correct state object to toggle the menu item closed', () => {
    const wrapper = shallow(
      <Course
        setTitle={() => {}}
        setParent={() => {}}
        classes={{}}
        match={{ params: {} }}
      />
    );

    expect(wrapper.instance().setCurrentExpanded(1)({ expanded: 1 })).toEqual({
      expanded: -1
    });
  });

  it('returns the correct state object to toggle the menu item open', () => {
    const wrapper = shallow(
      <Course
        setTitle={() => {}}
        setParent={() => {}}
        classes={{}}
        match={{ params: {} }}
      />
    );

    expect(wrapper.instance().setCurrentExpanded(0)({ expanded: 1 })).toEqual({
      expanded: 0
    });
  });
});
