import puppeteer from 'puppeteer';

describe('login page', () => {
  let browser;
  let page;

  beforeEach(async () => {
    // Setup puppeteer
    browser = await puppeteer.launch({
      headless: true
    });
    page = await browser.newPage();

    page.emulate({
      viewport: {
        width: 800,
        height: 600
      },
      userAgent: ''
    });

    // Goto homepage and wait for title
    await page.goto('http://127.0.0.1:8080/');
    await page.waitForSelector('.card h2');

    // Make sure we are on the login page
    const title = await page.$eval('.card h2', (e) => e.innerText);
    expect(title).toBe('Login');
  }, 10000);

  it(
    'displays an error message on existing email, wrong password',
    async () => {
      await page.type('#email', 'nevan@tanclan.ca');
      await page.type('#password', 'foobar');
      await page.click('[type="submit"]');

      await page.waitForSelector('.error');

      const errorMessage = await page.$eval('.error', (e) => e.innerText);
      expect(errorMessage).toBe('Invalid email/password combination.');
    },
    10000
  );

  it(
    'displays an error message on non-existant, well-formed email',
    async () => {
      await page.type('#email', 'example@tanclan.ca');
      await page.type('#password', 'foobar');
      await page.click('[type="submit"]');

      await page.waitForSelector('.error');

      const errorMessage = await page.$eval('.error', (e) => e.innerText);
      expect(errorMessage).toBe('Invalid email/password combination.');
    },
    10000
  );

  it(
    'catches empty email and password fields client-side',
    async () => {
      await page.click('[type="submit"]');

      await page.waitForSelector('.error');

      const errorMessage = await page.$eval('.error', (e) => e.innerText);
      expect(errorMessage).toBe('Email and Password fields cannot be blank.');
    },
    10000
  );

  it(
    'catches empty email field client-side',
    async () => {
      await page.type('#password', 'foobar');
      await page.click('[type="submit"]');

      await page.waitForSelector('.error');

      const errorMessage = await page.$eval('.error', (e) => e.innerText);
      expect(errorMessage).toBe('Email and Password fields cannot be blank.');
    },
    10000
  );

  it(
    'catches empty password field client-side',
    async () => {
      await page.type('#email', 'example@tanclan.ca');
      await page.click('[type="submit"]');

      await page.waitForSelector('.error');

      const errorMessage = await page.$eval('.error', (e) => e.innerText);
      expect(errorMessage).toBe('Email and Password fields cannot be blank.');
    },
    10000
  );

  it(
    'can successfully login with valid credentials',
    async () => {
      await page.type('#email', process.env.EMAIL);
      await page.type('#password', process.env.PASSWORD);
      await page.click('[type="submit"]');

      await page.waitForSelector('#app h1');

      const title = await page.$eval('#app h1', (e) => e.innerText);
      expect(title).toBe('All Courses');
    },
    10000
  );

  it(
    'can successfully login with capitlalized email addresses',
    async () => {
      await page.type(
        '#email',
        process.env.EMAIL[0].toUpperCase() + process.env.EMAIL.slice(1)
      );
      await page.type('#password', process.env.PASSWORD);
      await page.click('[type="submit"]');

      await page.waitForSelector('#app h1');

      const title = await page.$eval('#app h1', (e) => e.innerText);
      expect(title).toBe('All Courses');
    },
    10000
  );

  afterEach(async () => {
    // Close browser
    await browser.close();
  }, 10000);
});
