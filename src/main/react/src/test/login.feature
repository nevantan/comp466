Feature: Login page

  Background:
    Given we are on the login page

  Scenario: I attempt to login in with an existing email but incorrect password
    Given an existing email is entered
    But an incorrect password is entered
    When I submit the login form
    Then an error message is displayed
    And I am prompted to try logging in again

  Scenario: I attempt to login with an non-existant, well-formed email
    Given a non-existant email is entered
    And an arbitraty password is entered
    When I submit the login form
    Then an error message is displayed
    And I am prompted to try logging in again

  Scenario: I attempt to submit the form without filling any fields
    Given the username and password fields are blank
    When I submit the login form
    Then an error message is displayed
    And the email and password fields are highlighted
    And I am prompted to try logging in again

  Scenario: I attempt to submit the form without filling the email field
    Given the password field is filled
    But the email field is blank
    When I submit the login form
    Then an error message is displayed
    And the email field is highlighted
    And I am prompted to try logging in again

  Scenario: I attempt to submit the form without filling the password field
    Given the email field is filled
    But the password field is blank
    When I submit the login form
    Then an error message is displayed
    And the password field is highlighted
    And I am prompted to try logging in again

  Scenario: I attempt to login with valid credentials
    Given an existing email is entered
    And a correct password is entered
    When I submit the login form
    Then I should be logged in successfully
    And be on the All Courses page

  Scenario: I attempt to login with valid credentials and a capitalized email address
    Given an existing, capitalized email is entered
    And a correct password is entered
    When I submit the login form
    Then I should be logged in successfully
    And be on the All Courses page