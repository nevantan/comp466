// React
import React from 'react';
import ReactDOM from 'react-dom';

// GraphQL
import ApolloClient from 'apollo-boost';
import { Query, ApolloProvider } from 'react-apollo';
import gql from 'graphql-tag';
import resolvers from './graphql/resolvers';

// JWT
import jwtDecode from 'jwt-decode';

// Styles
import { ThemeProvider } from 'react-jss';
import theme from './styles/theme';

// Components
import Dashboard from './components/Dashboard';

// Get target element
const app = document.getElementById('app');

// Setup token auth
let token = app.getAttribute('data-token');

let redirect = window.location.href.indexOf('/auth') !== -1;
app.removeAttribute('data-token');
if (token === 'null') token = localStorage.getItem('token');

// Save token to cookie and localStorage
document.cookie = `jwt=${token}; expires=;`;
localStorage.setItem('token', token);

// Setup GraphQL client
const user_id = jwtDecode(token).sub;
const client = new ApolloClient({
  headers: {
    Authorization: token
  },
  clientState: {
    defaults: {
      user_id
    },
    resolvers
  }
});

// Render app
const jsx = (
  <ApolloProvider client={client}>
    <ThemeProvider theme={theme}>
      <React.Fragment>
        <Dashboard redirect={redirect} graphql={client} userid={user_id} />
      </React.Fragment>
    </ThemeProvider>
  </ApolloProvider>
);
ReactDOM.render(jsx, app);
redirect = false;

// <a
//       href="./dashboard"
//       onClick={(e) => {
//         e.preventDefault();
//         const d = new Date();
//         d.setTime(d.getTime() - 1000 * 60 * 60 * 24);
//         document.cookie = `jwt=;expires=${d.toGMTString()}`;
//         localStorage.removeItem('token');
//         window.location.href = './dashboard';
//       }}
//     >
//       Logout
//     </a>
