const color = {
  primary: '#0A2463', // Royal Blue
  secondary: '#456990', // Sapphire Blue
  highlight: '#D8315B', // Cherry
  light: '#FFFAFF', // Off-White
  dark: '#1E1B18', // Eerie Black
  pass: '#008800', // Green
  fail: '#ff0000' // Red
};

const font = {
  family: {
    header: '"Montserrat", sans-serif',
    body: '"Open Sans", sans-serif'
  },
  size: {
    xs: '1.4rem',
    s: '1.8rem',
    m: '2.4rem',
    l: '3.6rem',
    xl: '4.8rem'
  }
};

const spacing = {
  xs: '1.0rem',
  s: '1.6rem',
  m: '2.4rem',
  l: '3.6rem',
  xl: '4.8rem'
};

const common = {
  btnPrimary: {
    backgroundColor: color.primary,
    border: 0,
    borderRadius: '0.2rem',
    boxShadow: '0.1rem 0.1rem 0.1rem 0.1rem rgba(0, 0, 0, 0.5)',
    color: color.light,
    cursor: 'pointer',
    fontFamily: font.family.body,
    fontSize: font.size.s,
    padding: spacing.xs
  },
  btnDisabled: {
    backgroundColor: '#a4a4a4',
    border: 0,
    borderRadius: '0.2rem',
    boxShadow: '0.1rem 0.1rem 0.1rem 0.1rem rgba(0, 0, 0, 0.3)',
    color: color.light,
    cursor: 'pointer',
    fontFamily: font.family.body,
    fontSize: font.size.s,
    padding: spacing.xs
  },
  btnClose: {
    background: 'transparent',
    border: 0,
    color: '#666',
    fontSize: font.size.s,
    height: '1.8rem',
    position: 'absolute',
    right: spacing.xs,
    top: spacing.xs,
    width: '1.8rem'
  },
  overlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    height: '100%',
    left: 0,
    position: 'fixed',
    top: 0,
    width: '100%',
    zIndex: 10
  },
  modal: {
    backgroundColor: '#fff',
    border: '1px solid #ccc',
    borderRadius: '2px',
    display: 'flex',
    flexDirection: 'column',
    left: '50%',
    margin: '0 0 0 -15rem',
    padding: spacing.s,
    position: 'fixed',
    top: '8rem',
    width: '30rem',

    '& h2': {
      fontFamily: font.family.header,
      fontSize: font.size.m,
      fontWeight: 500,
      margin: `0 0 ${spacing.xs} 0`
    }
  }
};

export default {
  color,
  font,
  spacing,
  common
};
