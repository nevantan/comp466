const DotEnv = require('dotenv');
DotEnv.config({ path: '.env' });

const path = require('path');
const webpack = require('webpack');

module.exports = {
  entry: ['@babel/polyfill', path.resolve(__dirname, '.', 'src', 'app.js')],
  output: {
    path: path.resolve(__dirname, '..', 'webapp', 'dist'),
    filename: 'bundle.js'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        loader: 'babel-loader'
      }
    ]
  },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.EMAIL': JSON.stringify(process.env.EMAIL),
      'process.env.PASSWORD': JSON.stringify(process.env.PASSWORD)
    })
  ]
};
