module.exports = {
  setupFiles: ['raf/polyfill', '<rootDir>/jest.setup.js'],
  snapshotSerializers: ['enzyme-to-json/serializer'],
  testMatch: ['<rootDir>/src/**/*.test.js'],
  testPathIgnorePatterns: ['<rootDir>/node_modules/', '<rootDir>/*.setup.js'],
  collectCoverageFrom: [
    '<rootDir>/src/components/**/*.js',
    '!<rootDir>/**/style.js',
    '!<rootDir>/**/*.config.js',
    '!<rootDir>/**/*.setup.js',
    '!<rootDir>/**/theme.js',
    '!<rootDir>/node_modules/*'
  ],
  testURL: 'http://127.0.0.1/'
};
