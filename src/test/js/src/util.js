const query = async (ctx, query) => {
  return await ctx({
    method: 'post',
    url:'/graphql',
    params: {
      query
    },
    paramsSerializer:(params) => {
      let data = '';
      Object.keys(params).forEach((key) => {
        data += `${key}=${encodeURI(params[key])}&`
      });
      return data.slice(0, -1);
    }
  }).then((response) => {
    return response.data;
  }).catch((e) => {
    console.error(e);
    return false;
  });
};

export { query };