import { query } from './util';

const axios = require('axios');
const ctx = axios.create({
  baseURL: 'http://127.0.0.1:8888/comp466',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
});

test('fetch ecma200 unit 1 lesson 1 all sections', async () => {
  const expected = {
    data: {
      course: {
        unit: {
          lesson: {
            sections: [
              { title: 'The Console' },
              { title: 'Data Types' },
              { title: 'Strings: How To' }
            ]
          }
        }
      }
    }
  };

  const result = await query(
    ctx,
    `
    query {
      course(file:"ecma200.xml") {
        unit(id:"bar") {
          lesson(id:"baz") {
            sections {
              title
            }
          }
        }
      }
    }
  `
  );

  expect(result).toEqual(expected);
});
