import { query } from './util';

const axios = require('axios');
const ctx = axios.create({
  baseURL: 'http://127.0.0.1:8888/comp466',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
});

test('fetch ecma200 unit 1 single lesson', async () => {
  const expected = {
    data: {
      course: {
        unit: {
          lesson: {
            id: 'baz',
            title: 'Lesson 1: Introduction'
          }
        }
      }
    }
  };

  const result = await query(
    ctx,
    `
    query {
      course(file:"ecma200.xml") {
        unit(id:"bar") {
          lesson(id:"baz") {
            id, title
          }
        }
      }
    }
  `
  );

  expect(result).toEqual(expected);
});

test('fetch ecma200 unit 1 all lessons', async () => {
  const expected = {
    data: {
      course: {
        unit: {
          lessons: [
            {
              id: 'baz',
              title: 'Lesson 1: Introduction'
            }
          ]
        }
      }
    }
  };

  const result = await query(
    ctx,
    `
    query {
      course(file:"ecma200.xml") {
        unit(id:"bar") {
          lessons {
            id, title
          }
        }
      }
    }
  `
  );

  expect(result).toEqual(expected);
});
