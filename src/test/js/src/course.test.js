import { query } from './util';

const axios = require('axios');
const ctx = axios.create({
  baseURL: 'http://127.0.0.1:8888/comp466',
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded'
  }
});

test('fetch ecma200 metadata', async () => {
  const expected = {
    data: {
      course: {
        id: 'ecma200',
        code: 'ECMA200',
        title: 'Introduction to JavaScript'
      }
    }
  };

  const result = await query(
    ctx,
    `
    query {
      course(file:"ecma200.xml") {
        id, title, code
      }
    }
  `
  );

  expect(result).toEqual(expected);
});

test('fetch ecma200 single unit', async () => {
  const expected = {
    data: {
      course: {
        unit: {
          id: 'bar',
          title: 'Unit 1: Basic Syntax'
        }
      }
    }
  };

  const result = await query(
    ctx,
    `
    query {
      course(file:"ecma200.xml") {
        unit(id:"bar") {
          id, title
        }
      }
    }
  `
  );

  expect(result).toEqual(expected);
});

test('fetch ecma200 all units', async () => {
  const expected = {
    data: {
      course: {
        units: [
          {
            id: 'bar',
            title: 'Unit 1: Basic Syntax'
          }
        ]
      }
    }
  };

  const result = await query(
    ctx,
    `
    query {
      course(file:"ecma200.xml") {
        units {
          id, title
        }
      }
    }
  `
  );

  expect(result).toEqual(expected);
});
